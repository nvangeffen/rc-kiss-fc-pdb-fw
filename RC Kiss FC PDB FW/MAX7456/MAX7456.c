/*
 * MAX7456.c
 *
 * Created: 24/06/2016 5:37:46 p.m.
 *  Author: Ned
 */ 

#include "MAX7456.h"

uint8_t virtCols, virtRows;

void OSD_WriteReg(uint8_t reg, uint8_t data) {
	SS = HIGH;
	SS = LOW;
	WriteSpi(reg);
	WriteSpi(data);
	SS = HIGH;
}

void OSD_WriteRegAI(uint8_t data) {
	SS = HIGH;
	SS = LOW;
	WriteSpi(data);
	SS = HIGH;
}

uint8_t OSD_ReadReg(uint8_t reg) {
	uint8_t retVal;
	
	SS = HIGH;
	SS = LOW;
	WriteSpi(reg);
	retVal = WriteSpi(0x00);
	SS = HIGH;
	
	return retVal;
}

uint8_t OSD_IsValid(uint8_t col, uint8_t row) {
	uint8_t remCols, remRows;
	
	if ((virtCols == OSD_COLS) && (virtRows == OSD_ROWS))		return TRUE;
	
	remCols = OSD_COLS - virtCols;
	remRows = OSD_ROWS - virtRows;
	
	if (col < (remCols / 2)) return FALSE;
	if (row < (remRows / 2)) return FALSE;
	
	if (col >= (virtCols + (remCols / 2))) return FALSE;
	if (row >= (virtRows + (remRows / 2))) return FALSE;
	
	return TRUE;	
}

void OSD_SendString(uint8_t col, uint8_t row, char *data) {
	uint16_t address;
	uint8_t remCols, remRows;
	
	remCols = OSD_COLS - virtCols;
	remRows = OSD_ROWS - virtRows;
	
	col += (remCols / 2);
	row += (remRows / 2);
	
	address = row;
	address *= OSD_COLS;
	address += col;
	
	OSD_WriteReg(DM_ADDRH_WRITE, address >> 8);
	OSD_WriteReg(DM_ADDRL_WRITE, address);
	OSD_WriteReg(DM_MODE_WRITE, 0x41);
	
	while (*data) {
		if (OSD_IsValid(col, row)) OSD_WriteRegAI(*data++);
		else						   OSD_WriteRegAI(' ');
		
		col++;
		if (col >= OSD_COLS) {
			row++;
			col -= OSD_COLS;
			if (row >= OSD_ROWS) {
				row -= OSD_ROWS;
				OSD_WriteRegAI(0xFF);
				
				address = row;
				address *= OSD_COLS;
				address += col;
				
				OSD_WriteReg(DM_ADDRH_WRITE, address >> 8);
				OSD_WriteReg(DM_ADDRL_WRITE, address);
				OSD_WriteReg(DM_MODE_WRITE, 0x41);
			}
		}
	}
	OSD_WriteRegAI(0xFF);
}

void SetupOSD(void) {
	while (OSD_ReadReg(STATUS_READ) == STATUS_40_RESET_BUSY) ;
	
	OSD_SetScreenSize(29,15);
	
	OSD_WriteReg(OSDBL_WR, OSD_ReadReg(OSDBL_WR) & (~(1 << 4)));
	OSD_WriteReg(VIDEO_MODE_0_WRITE, VIDEO_MODE_0_40_PAL | VIDEO_MODE_0_08_EnOSD);
	
	//OSD_SendString(7,6, "King of Kings!");
}

void OSD_Reset(void) {
	OSD_WriteReg(VIDEO_MODE_0_WRITE, VIDEO_MODE_0_02_Reset);
}

void OSD_SetScreenSize(uint8_t cols, uint8_t rows) {
	uint8_t remCols, remRows;
	uint8_t offsetH, offsetV;
	
	virtCols = cols;
	virtRows = rows;
	
	if (virtCols > OSD_COLS)	virtCols = OSD_COLS;
	if (virtRows > OSD_ROWS)		virtRows = OSD_ROWS;
	
	remCols = virtCols - OSD_COLS;
	remRows = virtRows - OSD_ROWS;
	
	if (remCols % 2) offsetH = CHAR_SIZE_H / 2;
	else             offsetH = 0;
	if (remRows % 2) offsetV = CHAR_SIZE_V / 2;
	else             offsetV = 0;
	
	OSD_SetOffset(offsetH, offsetV);
}

void OSD_SetOffset(int8_t hor, int8_t vert) {
	uint8_t hOffset, vOffset;
	
	hor  = BIND(hor,  (1-VIDEO_OFFSET_H_MID), (VIDEO_OFFSET_H_MID - 1));
	vert = BIND(vert, (1-VIDEO_OFFSET_V_MID), (VIDEO_OFFSET_V_MID - 1));
	
	hOffset = hor  + VIDEO_OFFSET_H_MID;
	vOffset = vert + VIDEO_OFFSET_V_MID;
	
	OSD_WriteReg(VIDEO_OFFSET_H_WRITE, hOffset);
	OSD_WriteReg(VIDEO_OFFSET_V_WRITE, vOffset);
	
}