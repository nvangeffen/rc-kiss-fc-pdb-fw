/*
 * Buttons.h
 *
 * Created: 30/06/2016 11:58:47 a.m.
 *  Author: Ned van Geffen - King of Kings
 */ 


#ifndef BUTTONS_H_
#define BUTTONS_H_

#include <avr/io.h>
#include "ConfigButtons.h"

// struct prototypes
struct buttonsModule;
struct buttonsPin;
struct buttonsConfig;

// function callback prototype
typedef void (*buttonsCallback_t)(struct buttonsModule *const module, uint32_t buttons);

// Enum for callback types.
enum buttonsCallback {
	BUTTONS_CALLBACK_PRESSED,	// Callback for buttons pressed.
	BUTTONS_CALLBACK_HELD,		// Callback for buttons held.
	BUTTONS_CALLBACK_RELEASED,	// Callback for buttons released.
	BUTTONS_CALLBACK_N,			// Number of callback types.
};

// Struct of pins used in button module
struct buttonsPin {
	volatile uint8_t *pinReg;	// The register the pin is read from
	uint8_t regMask;				// The mask of which pin we are reading
	uint8_t activeLow;			// whether the pin is active high or low
};

// Button Module Struct. Holds eveything needed to check button statuses
struct buttonsModule {
	// USer Settings
	uint16_t debouncePress;								// The debounce for a single press.
	uint16_t debounceHold;								// The debounce for a button hold.
	uint8_t  repeatEnabled;								// Button holds can either trigger the micro once, or repeated at an interval.
	uint16_t repeatMax;									// The start value of repeat debouncing when first holding a button.
	uint16_t repeatMin;									// The lowest value of repeat debouncing when holding a button.
	uint8_t  rampEnabled;								// Repeat button holds can ramp up in frequency for big lists. This enables that ramping
	uint8_t  rampSpeed;									// The repeat value decreases by the current repeat value >> by this value (aka 3 means it decreases by 1/8th every time)
	
	// Internally Used Variables
	volatile uint16_t *curTime;							// Pointer to time. Preferably a millisecond timer.
	uint16_t time;										// The of the last button change
	uint16_t repeat;										// Time between button holds
	uint16_t lastTime;									// Last time we updated the buttons
	uint8_t  heldFlag;									// Flag to show a button held has happened
	uint8_t  decreaseFlag;								// Flag to show one or more buttons has been released
	uint8_t  lastCount;									// Number of buttons pressed at last count (popcount)
	struct buttonsModule *next;							// The next module in the linked list
	
	buttonsCallback_t callback[BUTTONS_CALLBACK_N];		// Callbacks for the various available callbacks
	#if BUTTONS_CALLBACK_N <= 8
	uint8_t registeredCallback;							// Bit field that holds which callbacks have something registered in them
	uint8_t enabledCallback;								// Bit field that holds which callbacks are enabled
	#elif BUTTONS_CALLBACK_N <= 16
	uint16_t registeredCallback;
	uint16_t enabledCallback;
	#elif BUTTONS_CALLBACK_N <= 32
	uint32_t registeredCallback;
	uint32_t enabledCallback;
	#endif
	
	struct buttonsPin pins[MAX_BUTTONS_PER_MODULE];		// Struct array of which pins on which port represent which bit, and if the yare active high/low/
	
	#if MAX_BUTTONS_PER_MODULE <= 8
	uint8_t enabledButtons;								// Bitfield of the enabled buttons in the module
	uint8_t pressed;										// Bitfield of the pressed buttons (cleared by callback after callback)
	uint8_t held;										// Bitfield of the held buttons (cleared by callback after callback)
	uint8_t released;									// Bitfield of the released buttons (cleared by callback after callback)
	uint8_t lastStatus;									// Bitfield of the button state last time the code ran
	#elif MAX_BUTTONS_PER_MODULE <= 16
	uint16_t enabledButtons;
	uint16_t pressed;
	uint16_t held;
	uint16_t released;
	uint16_t lastStatus;
	#elif MAX_BUTTONS_PER_MODULE <= 32
	uint32_t enabledButtons;
	uint32_t pressed;
	uint32_t held;
	uint32_t released;
	uint32_t lastStatus;
	#endif
};

// Config struct with all user settings to setup for a module
struct buttonsConfig {
	uint16_t debouncePress;				// The debounce for a single press.
	uint16_t debounceHold;				// The debounce for a button hold.
	uint8_t  repeatEnabled;				// Button holds can either trigger the micro once, or repeated at an interval.
	uint16_t repeatMax;					// The start value of repeat debouncing when first holding a button.
	uint16_t repeatMin;					// The lowest value of repeat debouncing when holding a button.
	uint8_t  rampEnabled;				// Repeat button holds can ramp up in frequency for big lists. This enables that ramping
	uint8_t  rampSpeed;					// The repeat value decreases by the current repeat value >> by this value (aka 3 means it decreases by 1/8th every time)
};


/***** ButtonsGetConfigDefaults
 * Set all the user setting defaults.
 ----------
 * @param - config,	Pointer to a config struct with all the user settable settings to set to defaults.
 *****/
static inline void ButtonsGetConfigDefaults(struct buttonsConfig *const config) {
	config->debouncePress = 30;			// 30mS debounce for single press	
	config->debounceHold = 600;			// 600mS debounce for a button hold
	config->repeatEnabled = 1;			// Enable multi events for button holds
	config->repeatMax = 500;				// First multi hold 500mS delay
	config->repeatMin = 25;				// Lowest multi hold delay when using the ramp feature
	config->rampEnabled = 1;				// Ramping of held button events enabled
	config->rampSpeed = 3;				// Ramp delay decreases by 1/(2^3);
}

/***** ButtonsInit
 * Set all the user settings to a module.
 * And clear all other bits and bobs.
 ----------
 * @param - module, The module being set up
 * @param - time,	Pointer to a time variable. Preferably uint16_t incremented at 1KHz (1mS)
 * @param - config,	Config struct with all the user settable settings.
 *****/
void ButtonsInit(struct buttonsModule *const module, uint16_t *time, const struct buttonsConfig *const config);

/***** ButtonsAddModule
 * This adds the module to the linked list so the task handler knows to update it.
 ----------
 * @param - module, The module to add to the list
 *****/
void ButtonsAddModule(struct buttonsModule *const module);

/***** ButtonsRegisterCallback
 * Set the callback function pointer
 ----------
 * @param - module,			The module to set the callback in
 * @param - callbackFunc,	Function pointer to the callback handler
 * @param - callbackType,	Type of callback it is we are setting
 *****/
void ButtonsRegisterCallback(struct buttonsModule *const module, buttonsCallback_t callbackFunc, enum buttonsCallback callbackType);

/***** ButtonsUnregisterCallback
 * Clear the callback function pointer
 ----------
 * @param - module,			The module to clear the callback in
 * @param - callbackType,	Type of callback it is we are clearing
 *****/
void ButtonsUnregisterCallback(struct buttonsModule *const module, enum buttonsCallback callbackType);

/***** ButtonsEnableCallback
 * Enable a callback function
 ----------
 * @param - module,			The module to enable the callback in
 * @param - callbackType,	Type of callback it is we are enabling
 *****/
void ButtonsEnableCallback(struct buttonsModule *const module, enum buttonsCallback callbackType);

/***** ButtonsDisableCallback
 * Disable a callback function
 ----------
 * @param - module,			The module to disable the callback in
 * @param - callbackType,	Type of callback it is we are disabling
 *****/
void ButtonsDisableCallback(struct buttonsModule *const module, enum buttonsCallback callbackType);

/***** ButtonsSetButton
 * Set what the button is in the specific index of a module
 ----------
 * @param - module,		The module to enable the button in
 * @param - pinReg,		The register pointer of where the button is
 * @param - bitNum,		The index in the register where the button is we want.
 * @param - activeLow,	Whether the button is active low or not. (1 = active low, 0 = active high)
 * @param - buttonNum,	Button index we are setting
 *****/
void ButtonsSetButton(struct buttonsModule *const module, volatile uint8_t *pinReg, uint8_t bitNum, uint8_t activeLow, uint8_t buttonNum);

/***** ButtonsEnableButton
 * Enable a button in the module
 ----------
 * @param - module,		The module to enable the button in
 * @param - buttonNum,	Button index we are enabling
 *****/
void ButtonsEnableButton(struct buttonsModule *const module, uint8_t buttonNum);

/***** ButtonsDisableButton
 * Disable a button in the module
 ----------
 * @param - module,		The module to disable the button in
 * @param - buttonNum,	Button index we are disabling
 *****/
void ButtonsDisableButton(struct buttonsModule *const module, uint8_t buttonNum);

/***** ButtonTaskHandler
 * The main task that reads all the buttons for all the modules and
 * figures out if there are any button presses or not.
 ----------
 *****/
void ButtonTaskHandler(void);


#endif /* BUTTONS_H_ */