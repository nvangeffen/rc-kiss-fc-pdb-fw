/*
* light weight WS2812 lib V2.0b
*
* Controls WS2811/WS2812/WS2812B RGB-LEDs
* Author: Tim (cpldcpu@gmail.com)
*
* Jan 18th, 2014  v2.0b Initial Version
*
* License: GNU GPL v2 (see License.txt)
*/

#include "WS2812.h"
#include "WS2812_config.h"

#include <util/atomic.h>
#include <util/delay.h>
#include <avr/interrupt.h>

static void sendBit(uint8_t bitVal) {	
	if (bitVal) {	
		asm volatile (
			"sbi %[port], %[bit] \n\t"				// Set the output bit
			".rept %[onCycles] \n\t"                                // Execute NOPs to delay exactly the specified number of cycles
			"nop \n\t"
			".endr \n\t"
			"cbi %[port], %[bit] \n\t"                              // Clear the output bit
			".rept %[offCycles] \n\t"                               // Execute NOPs to delay exactly the specified number of cycles
			"nop \n\t"
			".endr \n\t"
			::
			[port]		"I" (_SFR_IO_ADDR(PIXEL_PORT)),
			[bit]		"I" (PIXEL_BIT),
			[onCycles]	"I" (NS_TO_CYCLES(T1H) - 2),		// 1-bit width less overhead  for the actual bit setting, note that this delay could be longer and everything would still work
			[offCycles] "I" (NS_TO_CYCLES(T1L) - 2)			// Minimum interbit delay. Note that we probably don't need this at all since the loop overhead will be enough, but here for correctness
		);
	} 
	else {
		// **************************************************************************
		// This line is really the only tight goldilocks timing in the whole program!
		// **************************************************************************
		asm volatile (
			"sbi %[port], %[bit] \n\t"				// Set the output bit
			".rept %[onCycles] \n\t"				// Now timing actually matters. The 0-bit must be long enough to be detected but not too long or it will be a 1-bit
			"nop \n\t"                                              // Execute NOPs to delay exactly the specified number of cycles
			".endr \n\t"
			"cbi %[port], %[bit] \n\t"                              // Clear the output bit
			".rept %[offCycles] \n\t"                               // Execute NOPs to delay exactly the specified number of cycles
			"nop \n\t"
			".endr \n\t"
			::
			[port]		"I" (_SFR_IO_ADDR(PIXEL_PORT)),
			[bit]		"I" (PIXEL_BIT),
			[onCycles]	"I" (NS_TO_CYCLES(T0H) - 2),
			[offCycles]	"I" (NS_TO_CYCLES(T0L) - 2)
		);
	}	
}


static void sendByte( unsigned char byte ) {	
	PIXEL_DDR |= (1 << PIXEL_BIT);
	
	for (uint8_t bit = 0; bit < 8; bit++) {
		sendBit( !!(byte & 0x80) );		// MSb
		byte <<= 1;
	}
}

void WS_Show(void) {
	_delay_us( (RES / 1000UL) + 1);				// Round up since the delay must be _at_least_ this long (too short might not work, too long not a problem)
}

void WS_SetLed (led_t led) {
	sendByte(led.g);
	sendByte(led.r);
	sendByte(led.b);
}

void WS_SetLedRgb(uint32_t colour) {
	sendByte((colour >>  8) & 0xFF);
	sendByte((colour >> 16) & 0xFF);
	sendByte((colour >>  0) & 0xFF);
}

void WS_ConvertRGB(led_t * led, uint32_t colour) {
	led->r = (colour >> 16) & 0xFF;
	led->g = (colour >>  8) & 0xFF;
	led->b = (colour >>  0) & 0xFF;
}

void WS_ConvertIndex(led_t * led, uint16_t colIndex) {
	colIndex &= 0x03FF;
	
	if (colIndex <= 0x00FF) {
		led->g = (colIndex & 0x00FF);
		led->r = 0xFF - led->g;
		led->b = 0x00;
	}
	else if (colIndex <= 0x01FF) {
		led->b = (colIndex & 0x00FF);
		led->g = 0xFF - led->b;
		led->r = 0x00;
	}
	else if (colIndex <= 0x02FF) {
		led->r = (colIndex & 0x00FF);
		led->b = 0xFF - led->r;
		led->g = 0x00;
	}
}

void WS_NormaliseLed(led_t *led, const led_t *original) {
	uint16_t ledTotal, remainder;
	uint32_t ledMath;
	uint8_t brightIndex, loop;
	
	ledTotal = original->r + original->g + original->b;
	remainder = 0xFF;
				
	if (ledTotal > 0xFF) {
		for (brightIndex = loop = 0; loop < 3; loop++)
			if ((((uint8_t*)(original))[loop]) > (((uint8_t*)(original))[brightIndex]))
				brightIndex = loop;
		
		for (loop = 0; loop < 3; loop++) {
			ledMath = 0xFF;
			ledMath *= ((uint8_t*)(led))[loop];
			ledMath /= ledTotal;
			((uint8_t*)(led))[loop] = (uint8_t)(ledMath);
			remainder -= ((uint8_t*)(led))[loop];
		}
		
		((uint8_t*)(led))[brightIndex] += (!!remainder);
	}
}

void WS_LedBrightness(led_t * led, uint8_t brightness, uint8_t brightMax) {
	uint8_t brightIndex, loop;
	uint16_t ledMath, remainder;
	
	remainder = ((0xFF * brightness) / brightMax);
	
	for (brightIndex = loop = 0; loop < 3; loop++)
		if ((((uint8_t*)(led))[loop]) > (((uint8_t*)(led))[brightIndex]))
			brightIndex = loop;
	
	for (loop = 0; loop < 3; loop++) {
		ledMath = ((uint8_t*)(led))[loop];
		ledMath *= brightness;
		ledMath /= brightMax;
		((uint8_t*)(led))[loop] = (uint8_t)(ledMath);
		remainder -= ((uint8_t*)(led))[loop];
	}
		
	((uint8_t*)(led))[brightIndex] += (!!remainder);
}
