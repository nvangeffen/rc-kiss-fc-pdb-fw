/*
 * light_ws2812_config.h
 *
 * Created: 18.01.2014 09:58:15
 *
 * User Configuration file for the light_ws2812_lib
 *
 */ 


#ifndef WS2812_CONFIG_H_
#define WS2812_CONFIG_H_

#define WHITE		0xFFFFFF
#define RED			0xFF0000
#define ORANGE		0xFF8C00
#define YELLOW		0xFFFF00
#define GREEN		0x00FF00
#define CYAN		0x00FFFF
#define BLUE		0x0000FF
#define PURPLE		0xFF00FF

#define PIXELS 10  // Number of pixels in the string

#define PIXEL_PORT  PORTD  // Port of the pin the pixels are connected to
#define PIXEL_DDR   DDRD   // Port of the pin the pixels are connected to
#define PIXEL_BIT   5      // Bit of the pin the pixels are connected to

#endif /* WS2812_CONFIG_H_ */