/*
 * light weight WS2812 lib include
 *
 * Version 2.0a3  - Jan 18th 2014
 * Author: Tim (cpldcpu@gmail.com) 
 *
 * Please do not change this file! All configuration is handled in "ws2812_config.h"
 *
 * License: GNU GPL v2 (see License.txt)
 +
 */ 

#ifndef LIGHT_WS2812_H_
#define LIGHT_WS2812_H_

#include <avr/io.h>
#include "WS2812_config.h"

#define T1H  900    // Width of a 1 bit in ns
#define T1L  600    // Width of a 1 bit in ns

#define T0H  400    // Width of a 0 bit in ns
#define T0L  900    // Width of a 0 bit in ns

#define RES 6000    // Width of the low gap between bits to cause a frame to latch

#define NS_PER_SEC		(1000000000L)          // Note that this has to be SIGNED since we want to be able to check for negative values of derivatives
#define CYCLES_PER_SEC	(F_CPU)
#define NS_PER_CYCLE		( NS_PER_SEC / CYCLES_PER_SEC )
#define NS_TO_CYCLES(n) ( (n) / NS_PER_CYCLE )

typedef struct led_t {uint8_t g,r,b;} led_t;

void WS_SetLedRgb(uint32_t colour);
void WS_SetLed(led_t led);
void WS_Show(void);
void WS_ConvertRGB(led_t * led, uint32_t colour);
void WS_ConvertIndex(led_t * led, uint16_t colIndex);
void WS_NormaliseLed(led_t *led, const led_t *original);
void WS_LedBrightness(led_t * led, uint8_t brightness, uint8_t brightMax);

#endif /* LIGHT_WS2812_H_ */