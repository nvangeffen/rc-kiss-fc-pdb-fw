/*
 * ADC.c
 *
 * Created: 24/06/2016 12:18:06 p.m.
 *  Author: Ned
 */ 

#include <avr/io.h>
#include "..\Header Files\includes.h"

void SetupADC(void) {
	ADCSRA = 0xC7;	// ADC Enable, start conversion and prescaler of 128. 16e6/128 = 125khz
	ADCSRB = 0x00;	// Set to free running mode and 10 bit adc
	ADMUX = 0x40;	// setup Avcc
}

uint16_t ReadADC(uint8_t channel) {
	unsigned int retValue, tempValue;
	
	channel &= 0x0F;

	ADMUX &= 0xF0;
	ADMUX |= (channel & 0x0F);				// Set the Channel
	ADCSRA |= BIT(ADEN) | BIT(ADSC);			// Start Conversion

	while (!(ADCSRA & (1 << ADIF)));			// wait for conversion to finish
	ADCSRA |= (1 << ADIF); 					// clear the finished flag
	retValue = ADCL;
	tempValue = ADCH;
	tempValue <<= 8;
	retValue |= tempValue;

	return retValue;							// return the reading
}

void AverageADC(adc_t *adc, uint8_t avg) {
	if (adc->tot == 0) {
		adc->tot = (adc->raw << avg);
		adc->tot -= adc->raw;
	}
	
	adc->tot += adc->raw;
	adc->avg = adc->tot >> avg;
	adc->tot -= adc->avg;
}