/*
 * ADC.h
 *
 * Created: 24/06/2016 12:17:58 p.m.
 *  Author: Ned
 */ 


#ifndef ADC_H_
#define ADC_H_

typedef struct {
	uint16_t raw;
	uint16_t avg;
	uint16_t tot;
} adc_t;

void SetupADC(void);
uint16_t ReadADC(uint8_t channel);
void AverageADC(adc_t *adc, uint8_t avg);

#endif /* ADC_H_ */