/*
 * SoftSerial.c
 *
 * Created: 23/07/2016 5:44:38 p.m.
 *  Author: Ned
 */ 

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "..\Header Files\NedsStandardHeader.h"

#define DDR_TX					REGISTER_BIT(DDRD,3)
#define DATA_TX					REGISTER_BIT(PORTD,3)
#define DATA_RX					REGISTER_BIT(PIND,3)

#define SOFT_SERIAL_PC_FALLING	(EIMSK = 0x02)
#define SOFT_SERIAL_PC_OFF		(EIMSK = 0x00)
#define SOFT_SERIAL_PC_CLEAR	(EIFR = 0x02)

#define SOFT_SERIAL_INT_OFF		(TIMSK1 = 0x00)
#define SOFT_SERIAL_INT_RX		(TIMSK1 = 0x04)
#define SOFT_SERIAL_INT_TX		(TIMSK1 = 0x02)

#define SOFT_SERIAL_BUSY_TX		(TIMSK1 & 0x02)
#define SOFT_SERIAL_BUSY_RX		(TIMSK1 & 0x04)
#define SOFT_SERIAL_BUSY		(SOFT_SERIAL_BUSY_TX | SOFT_SERIAL_BUSY_RX)

#define CLEAR_INT				TIFR1 = 0x07
#define RESET_TIMER				TCNT1 = 0x00

#define BUFF_SIZE	32
#define BUFF_MASK	(BUFF_SIZE - 1)

enum transferBit_t {
	START_BIT = 0,
	DATA_0,
	DATA_1,
	DATA_2,
	DATA_3,
	DATA_4,
	DATA_5,
	DATA_6,
	DATA_7,
	STOP_BIT0,
	STOP_BIT1,
	MAX_DATA_BITS,
};

volatile uint8_t softTxBuffer[BUFF_SIZE];
volatile uint8_t softRxBuffer[BUFF_SIZE];
volatile uint8_t softTxTail, softTxHead;
volatile uint8_t softRxTail, softRxHead;
volatile uint8_t txBit, rxBit;

uint8_t SoftSerialSending(void) {
	return SOFT_SERIAL_BUSY_TX;
}

uint8_t SoftSerialReceiving(void) {
	return SOFT_SERIAL_BUSY_RX;
}

uint8_t SoftSerialBusy(void) {
	return SOFT_SERIAL_BUSY;
}

void SoftSerialScewRxFast(void) {
	OCR1B = OCR1A >> 2;		// 1/4 point for reading...
}

void SoftSerialScewRxNormal(void) {
	OCR1B = OCR1A >> 1;		// 1/2 point for reading...
}

void SoftSerialScewRxSlow(void) {
	uint16_t tempVal = OCR1A >> 2;
	OCR1B = tempVal * 3;		// 3/4 point for reading...
}

void SetupSoftSerial(uint16_t baud) {
	DDR_TX = 0;
	DATA_TX = 1;
	
	// Timer
	TCCR1A = 0x00;
	TCCR1B = 0x0B;	// 16e6/64/4800=52.08333
	OCR1A = ((F_CPU/64)/baud) - 1;
	SoftSerialScewRxNormal();
	SOFT_SERIAL_INT_OFF;
	
	// Setup pin change interrupt
	EICRA = ((0x02) << ISC10);
	SOFT_SERIAL_PC_CLEAR;
	SOFT_SERIAL_PC_FALLING;
	
 	txBit = START_BIT;
	rxBit = START_BIT;
}

uint8_t SoftSerialAvailable(void) {
	return (BUFF_MASK + softRxHead - softRxTail) % BUFF_MASK;
}

uint8_t SoftGetC(void) {
	uint8_t retVal;
	
	if (SoftSerialAvailable()) {
		retVal = softRxBuffer[softRxTail];
		softRxTail++;
		softRxTail &= BUFF_MASK;
	}
	else retVal = 0;
	
	return retVal;
}

void SoftPutC(uint8_t c) {
	softTxBuffer[softTxHead] = c;
	softTxHead++;
	softTxHead &= BUFF_MASK;
	
	if (softTxHead == softTxTail) {
		softTxTail++;
		softTxTail &= BUFF_MASK;
	}
	
	if (!SOFT_SERIAL_BUSY) {
		DDR_TX = 1;
		DATA_TX = 1;
		RESET_TIMER;
		CLEAR_INT;
		SOFT_SERIAL_INT_TX;
		SOFT_SERIAL_PC_OFF;
	}
}

void SoftPutS(char *s) {
	while (*s)
		SoftPutC(*s++);
}

void SoftPutA(uint8_t *array, uint8_t noOfBytes) {
	while (noOfBytes--)
		SoftPutC(*array++);
}

ISR(TIMER1_COMPA_vect) {	
	//Send data
	switch (txBit++) {
		case START_BIT:			DATA_TX = 0;		break;
		case DATA_0:			DATA_TX = !!(softTxBuffer[softTxTail] & 0x01);		break;
		case DATA_1:			DATA_TX = !!(softTxBuffer[softTxTail] & 0x02);		break;
		case DATA_2:			DATA_TX = !!(softTxBuffer[softTxTail] & 0x04);		break;
		case DATA_3:			DATA_TX = !!(softTxBuffer[softTxTail] & 0x08);		break;
		case DATA_4:			DATA_TX = !!(softTxBuffer[softTxTail] & 0x10);		break;
		case DATA_5:			DATA_TX = !!(softTxBuffer[softTxTail] & 0x20);		break;
		case DATA_6:			DATA_TX = !!(softTxBuffer[softTxTail] & 0x40);		break;
		case DATA_7:			DATA_TX = !!(softTxBuffer[softTxTail] & 0x80);		break;
		case STOP_BIT0:		
		case STOP_BIT1:		DATA_TX = 1;		break;
		default:
			DATA_TX = 1;
			txBit = START_BIT;
			softTxTail++;
			softTxTail &= BUFF_MASK;
			
			if (softTxTail == softTxHead) {
				SOFT_SERIAL_INT_OFF;
				DDR_TX = 0;
				SOFT_SERIAL_PC_CLEAR;
				SOFT_SERIAL_PC_FALLING;
			}
	}
}

ISR(TIMER1_COMPB_vect) {
	static uint8_t error;
	
	// Read Data
	switch (rxBit++) {
		case START_BIT:			if (DATA_RX != 0) error |= (1 << 0); break;
		case DATA_0:			softRxBuffer[softRxHead]  = (DATA_RX << 0);		break;
		case DATA_1:			softRxBuffer[softRxHead] |= (DATA_RX << 1);		break;
		case DATA_2:			softRxBuffer[softRxHead] |= (DATA_RX << 2);		break;
		case DATA_3:			softRxBuffer[softRxHead] |= (DATA_RX << 3);		break;
		case DATA_4:			softRxBuffer[softRxHead] |= (DATA_RX << 4);		break;
		case DATA_5:			softRxBuffer[softRxHead] |= (DATA_RX << 5);		break;
		case DATA_6:			softRxBuffer[softRxHead] |= (DATA_RX << 6);		break;
		case DATA_7:			softRxBuffer[softRxHead] |= (DATA_RX << 7);		break;
		case STOP_BIT0:			if (DATA_RX != 1) error |= (1 << 1);			break;
		case STOP_BIT1:			if (DATA_RX != 1) error |= (1 << 2);
		default:
			//if (error)	softRxBuffer[softRxHead] = 0;
			if (!error) {
				softRxHead++;
				softRxHead &= BUFF_MASK;
			}
			
			if (softRxTail == softRxHead) {
				softRxTail++;
				softRxTail &= BUFF_MASK;
			}
			
			rxBit = START_BIT;
			error = 0x00;
			
			SOFT_SERIAL_INT_OFF;
			SOFT_SERIAL_PC_CLEAR;
			SOFT_SERIAL_PC_FALLING;
	}
}

ISR(INT1_vect) {
	// Falling Edge!
	SOFT_SERIAL_PC_OFF;
	
	RESET_TIMER;
	CLEAR_INT;
	SOFT_SERIAL_INT_RX;
	
	rxBit = START_BIT;	
}
