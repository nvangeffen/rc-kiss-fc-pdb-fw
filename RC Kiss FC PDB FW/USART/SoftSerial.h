/*
 * SoftSerial.h
 *
 * Created: 23/07/2016 5:44:26 p.m.
 *  Author: Ned
 */ 


#ifndef SOFTSERIAL_H_
#define SOFTSERIAL_H_

uint8_t SoftSerialBusy(void);
uint8_t SoftSerialSending(void);
uint8_t SoftSerialReceiving(void);
uint8_t SoftSerialAvailable(void);

void SetupSoftSerial(uint16_t baud);
void SoftSerialScewRxFast(void);
void SoftSerialScewRxNormal(void);
void SoftSerialScewRxSlow(void);

uint8_t SoftGetC(void);
void SoftPutC(uint8_t c);
void SoftPutS(char *s);
void SoftPutA(uint8_t *array, uint8_t noOfBytes);

#endif /* SOFTSERIAL_H_ */