/*
 * SPI.c
 *
 * Created: 24/06/2016 5:49:06 p.m.
 *  Author: Ned
 */ 

#include <avr/io.h>
#include "SPI.h"

void SetupSPI(void) {
	SPCR = 0x50;
	SPSR = 0x01;
}

uint8_t WriteSpi(uint8_t data) {
	SPDR = data;
	while ((SPSR & (1 << SPIF)) == 0) ;
	return SPDR;
}

uint8_t ReadSpi(void) {
	SPDR = 0x00;
	while ((SPSR & (1 << SPIF)) == 0) ;
	return SPDR;
}