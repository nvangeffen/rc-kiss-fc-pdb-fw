/*
 * SPI.h
 *
 * Created: 24/06/2016 5:49:16 p.m.
 *  Author: Ned
 */ 


#ifndef SPI_H_
#define SPI_H_

void SetupSPI(void);
uint8_t WriteSpi(uint8_t data);
uint8_t ReadSpi(void);

#endif /* SPI_H_ */