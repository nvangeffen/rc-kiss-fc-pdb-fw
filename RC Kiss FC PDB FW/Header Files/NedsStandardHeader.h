/************************************* START OF PROGRAM COMMENTS *******************************
* Name:			Neds Standard Header
* Version:		V1.0
* Created:      07/04/08
* Last Mod:		02/03/11
* Author:       Ned van Geffen
* Purpose:      Define some things ned often uses
************************************************************************************************/

/************************************* KNOWN BUGS **********************************************
* 
************************************************************************************************/

/************************************* NOTES ***************************************************
* 
************************************************************************************************/

#ifndef _NED_STANDARD_HEADER_INCLUDED_
	#define _NED_STANDARD_HEADER_INCLUDED_

	typedef struct
	{
	  unsigned int bit0:1;
	  unsigned int bit1:1;
	  unsigned int bit2:1;
	  unsigned int bit3:1;
	  unsigned int bit4:1;
	  unsigned int bit5:1;
	  unsigned int bit6:1;
	  unsigned int bit7:1;
	} _io_reg; 

	#define REGISTER_BIT(rg,bt) ((volatile _io_reg*)&rg)->bit##bt

#endif

#ifndef BIT
	#define BIT(b)	(1 << b)
#endif

#ifndef SET_BIT
	#define SET_BIT(sPort, sBit)	sPort |= BIT(sBit)
#endif

#ifndef CLEAR_BIT
	#define CLEAR_BIT(cPort, cBit)	cPort &= ~BIT(cBit)
#endif

#ifndef MAX
	#define MAX(a,b) 	(((a)>(b))?(a):(b))
#endif

#ifndef MIN
	#define MIN(a,b) 	(((a)<(b))?(a):(b))
#endif

#ifndef DIFF
	#define DIFF(a,b) 	(MAX(a,b) - MIN(a,b))
#endif

#ifndef BIND
	#define BIND(value, min, max) 	(MAX(MIN(value, max), min))
#endif

#ifndef TRUE
	#define TRUE		1
#endif

#ifndef FALSE
	#define FALSE		0
#endif

#ifndef OUT
	#define OUT	1
#endif

#ifndef IN
	#define IN		0
#endif

#ifndef HIGH
	#define HIGH		1
#endif

#ifndef LOW
	#define LOW			0
#endif

#ifndef ON
	#define ON			1
#endif

#ifndef OFF
	#define OFF			0
#endif

#ifndef ENABLED
	#define ENABLED		1
#endif

#ifndef DISABLED
	#define DISABLED	0
#endif

#ifndef NULL
	#define NULL	0
#endif