/*
 * Hardware.h
 *
 * Created: 24/06/2016 12:10:35 p.m.
 *  Author: Ned
 */ 

#include "..\Header Files\includes.h"

#ifndef HARDWARE_H_
#define HARDWARE_H_

// Hardware Specific Defines
#define MOSI_DDR	REGISTER_BIT(DDRB, 3)
#define MOSI		REGISTER_BIT(PORTB, 3)
#define MISO_DDR	REGISTER_BIT(DDRB, 4)
#define MISO_PUP	REGISTER_BIT(PORTB, 4)
#define MISO		REGISTER_BIT(PINB, 4)
#define SCK_DDR		REGISTER_BIT(DDRB, 5)
#define SCK			REGISTER_BIT(PORTB, 5)
#define SS_DDR		REGISTER_BIT(DDRB, 1)
#define SS			REGISTER_BIT(PORTB, 1)

#define LED_DDR		REGISTER_BIT(DDRB, 2)
#define LED			REGISTER_BIT(PORTB, 2)

#define RX_DDR		REGISTER_BIT(DDRD, 0)
#define RX_PUP		REGISTER_BIT(PORTD, 0)
#define TX_DDR		REGISTER_BIT(DDRD, 1)
#define TX			REGISTER_BIT(PORTD, 1)

#define VSYNC_DDR	REGISTER_BIT(DDRD, 2)
#define VSYNC_PUP	REGISTER_BIT(PORTD, 2)

#define ADC_BATT_V1	2
#define ADC_I_SENSE	1
#define ADC_BATT_V2	0
#define ADC_RSSI	3
#define ADC_WS		7


#endif /* HARDWARE_H_ */