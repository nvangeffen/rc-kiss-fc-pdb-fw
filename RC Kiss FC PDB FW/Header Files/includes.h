/*
 * includes.h
 *
 * Created: 24/06/2016 12:11:36 p.m.
 *  Author: Ned
 */ 


#ifndef INCLUDES_H_
#define INCLUDES_H_

#include "..\Header Files\Hardware.h"
#include "..\USART\uart.h"
#include "..\USART\SoftSerial.h"
#include "..\Header Files\NedsStandardHeader.h"
#include "..\ADC\ADC.h"
#include "..\SPI\SPI.h"
#include "..\MAX7456\MAX7456.h"
#include "..\WS LED\WS2812.h"
#include "..\Buttons\Buttons.h"
#include "..\Menu\MenuSystem.h"
#include "..\Memory\GlobalData.h"

extern adc_t battV1, battV2, rssi, iSense;

#endif /* INCLUDES_H_ */