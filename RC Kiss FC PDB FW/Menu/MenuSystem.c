/*
 * MenuSystem.c
 *
 * Created: 16/07/2016 4:04:00 p.m.
 *  Author: Ned
 */ 


#include "MenuSystem.h"
#include <stdio.h>
#include <string.h>
#include "..\WS LED\WS2812.h"

#define MENU_PARENT         MENU_READ_PTR(&Menu_GetCurrentMenu()->parent)
#define MENU_CHILD          MENU_READ_PTR(&Menu_GetCurrentMenu()->child)
#define MENU_NEXT           MENU_READ_PTR(&Menu_GetCurrentMenu()->next)
#define MENU_PREVIOUS       MENU_READ_PTR(&Menu_GetCurrentMenu()->prev)
#define MENU_ANCHOR         MENU_READ_PTR(&Menu_GetCurrentMenu()->anchor)
#define MENU_TIMEOUT        MENU_READ_PTR(&Menu_GetCurrentMenu()->timeoutMenu)
#define MENU_TIMEOUT_TIME   MENU_READ_PTR(&Menu_GetCurrentMenu()->timeoutTime)
//#define MENU_ENABLED        MENU_READ_PTR(&Menu_GetCurrentMenu()->enabled)
#define MENU_ENABLED        (*(uint8_t*)(MENU_READ_PTR(&Menu_GetCurrentMenu()->enabled)))
#define MENU_ON_ENTER       MENU_READ_PTR(&Menu_GetCurrentMenu()->onEnter)
#define MENU_ON_EXIT        MENU_READ_PTR(&Menu_GetCurrentMenu()->onExit)
#define MENU_ON_SAVE	        MENU_READ_PTR(&Menu_GetCurrentMenu()->onSave)
#define MENU_ON_DISPLAY     MENU_READ_PTR(&Menu_GetCurrentMenu()->onDisplay)
#define MENU_ON_CHANGE      MENU_READ_PTR(&Menu_GetCurrentMenu()->onChange)
#define MENU_VALUE          MENU_READ_PTR(&Menu_GetCurrentMenu()->val)
#define MENU_RULE_PTR       MENU_READ_PTR(&Menu_GetCurrentMenu()->rule)
#define MENU_VAL_TYPE       MENU_READ_PTR(&Menu_GetCurrentMenu()->valType)

#define TEMP_MENU_PARENT    MENU_READ_PTR(&Menu_GetTempMenu()->parent)
#define TEMP_MENU_CHILD     MENU_READ_PTR(&Menu_GetTempMenu()->child)
#define TEMP_MENU_NEXT      MENU_READ_PTR(&Menu_GetTempMenu()->next)
#define TEMP_MENU_PREVIOUS  MENU_READ_PTR(&Menu_GetTempMenu()->prev)
#define TEMP_MENU_ENABLED   (*(uint8_t*)(MENU_READ_PTR(&Menu_GetTempMenu()->enabled)))
#define TEMP_MENU_TIMEOUT   MENU_READ_PTR(&Menu_GetTempMenu()->timeout)

#define MENU_LIST_SIZE		(MENU_ROWS - MENU_TITLE)
#define MENU_LIST_INDEX		(menuIndex - displayIndex)

#define MENU_VALID(menu)		((menu != &NULL_MENU) && (menu != 0))
#define RULE_VALID(rule)		((rule != &NULL_RULE) && (rule != 0))
#define VALUE_VALID(val)		((val != &NULL_VALUE) && (val != 0))

int8_t anchorCount, menuSize, menuIndex, displayIndex, wrapMenu;

char menuScreen[MENU_ROWS][MENU_COLS];

uint8_t NULL_VALUE = 0;
valueRule_t NULL_RULE = {0};
const menuItem_t MENU_STORAGE NULL_MENU = {0};
uint8_t ENABLED_MENU = 1;
uint8_t DISABLED_MENU = 0;
static menuItem_t* currentMenuItem = &NULL_MENU;
static menuItem_t* tempMenuPtr = &NULL_MENU;
static menuItem_t* oldMenuPtr = &NULL_MENU;
/*static */uint16_t menuTimeout;

static void UpdateMenuScreens(void);
static void ScreenChange(void);

void Menu_Init(menuItem_t *initMenu, uint16_t time) {
	currentMenuItem = initMenu;	
	menuTimeout = time + (uint16_t)(MENU_TIMEOUT_TIME);
	ScreenChange();
	if (MENU_ON_DISPLAY != NULL)
		((menuCallback_t)(MENU_ON_DISPLAY))(Menu_GetCurrentMenu());
	else UpdateMenuScreens();
}

// returns a pointer to the current menu
menuItem_t* Menu_GetCurrentMenu(void) {
	return currentMenuItem;
}
// returns a pointer to the current menu
menuItem_t* Menu_GetTempMenu(void) {
	return tempMenuPtr;
}

uint8_t Menu_MenuValid(void) {
	return (MENU_VALID(Menu_GetCurrentMenu()));
}

void Menu_Clear(void) {
	int8_t loop;
	
	memset(menuScreen, ' ', sizeof(menuScreen));
	for (loop = 0; loop < MENU_ROWS; loop++) {
		menuScreen[loop][MENU_COLS - 1] = 0;
	}
}

void UpdateMenuScreens(void) {
	valueRule_t *rulePtr;
	int8_t loop;
	
	Menu_Clear();
	
	if (MENU_VALID(currentMenuItem)) {
		if (MENU_VALID(MENU_PARENT)) {
			tempMenuPtr = MENU_PARENT;
			snprintf(&menuScreen[0][0],MENU_COLS,"%-30" MENU_STRING_TYPE, (wchar_t*)tempMenuPtr->text);
		}
		else snprintf(&menuScreen[0][0],MENU_COLS,"%-30s", "Main Menu");
		
		if (VALUE_VALID(MENU_VALUE)) {
			rulePtr = MENU_RULE_PTR;
			tempMenuPtr = currentMenuItem;
			snprintf(&menuScreen[(MENU_ROWS >> 2) + MENU_TITLE][0],MENU_COLS,"-%" MENU_STRING_TYPE"%-30s", (wchar_t*)tempMenuPtr->text,"");
			
			switch ((enum valueType)(MENU_VAL_TYPE)) {
				case TYPE_UINT8:          snprintf(&menuScreen[(MENU_ROWS >> 2) + MENU_TITLE][MENU_COLS -  5], 5,  ":%3d%-5s",  ( uint8_t)((*rulePtr).currVal),"");    break;
				case TYPE_UINT16:         snprintf(&menuScreen[(MENU_ROWS >> 2) + MENU_TITLE][MENU_COLS -  7], 7,  ":%5d%-5s",  (uint16_t)((*rulePtr).currVal),"");    break;
				case TYPE_UINT32:         snprintf(&menuScreen[(MENU_ROWS >> 2) + MENU_TITLE][MENU_COLS - 12],12,":%10ld%-5s",  (uint32_t)((*rulePtr).currVal),"");    break;
				case TYPE_SINT8:          snprintf(&menuScreen[(MENU_ROWS >> 2) + MENU_TITLE][MENU_COLS -  5], 5,  ":%3d%-5s",  (  int8_t)((*rulePtr).currVal),"");    break;
				case TYPE_SINT16:         snprintf(&menuScreen[(MENU_ROWS >> 2) + MENU_TITLE][MENU_COLS -  7], 7,  ":%5d%-5s",  ( int16_t)((*rulePtr).currVal),"");    break;
				case TYPE_SINT32:         snprintf(&menuScreen[(MENU_ROWS >> 2) + MENU_TITLE][MENU_COLS - 12],12,":%10ld%-5s",  ( int32_t)((*rulePtr).currVal),"");    break;
				case TYPE_SINT8_PERC_1DP: snprintf(&menuScreen[(MENU_ROWS >> 2) + MENU_TITLE][MENU_COLS -  8], 8,":%3.1f%%%-5s", (float)((*rulePtr).currVal) / 10,"");  break;
				case TYPE_YES_NO:
					if ((int8_t)((*rulePtr).currVal))
					                      snprintf(&menuScreen[(MENU_ROWS >> 2) + MENU_TITLE][MENU_COLS -  5], 5,":Yes%-5s", "");
					else                  snprintf(&menuScreen[(MENU_ROWS >> 2) + MENU_TITLE][MENU_COLS -  5], 5,": No%-5s", "");
				default: break;
			}
		}
		else if (MENU_TITLE) {		
			tempMenuPtr = currentMenuItem;
			for (loop = menuIndex; loop > displayIndex; loop--) {
				do {
					if (MENU_VALID(TEMP_MENU_PREVIOUS)) {
						tempMenuPtr = TEMP_MENU_PREVIOUS;
					}
				} while ((TEMP_MENU_ENABLED == 0) && MENU_VALID(TEMP_MENU_PREVIOUS));
			}
				
			for (loop = 0; loop < MENU_LIST_SIZE; loop++) {					
				snprintf(&menuScreen[loop + MENU_TITLE][0],MENU_COLS,"-%" MENU_STRING_TYPE"%-30s", (wchar_t*)tempMenuPtr->text,"");
				do {
					if (MENU_VALID(TEMP_MENU_NEXT)) {
						tempMenuPtr = TEMP_MENU_NEXT;
					}
					else loop = MENU_LIST_SIZE;
				} while ((TEMP_MENU_ENABLED == 0) && (loop < MENU_LIST_SIZE));
				if ((!wrapMenu) && ((displayIndex + loop) >= menuSize)) loop = MENU_LIST_SIZE;
			}
				
			menuScreen[MENU_LIST_INDEX + MENU_TITLE][0] = '>';		
		}
		// else needs to be fixed up
	}
}

void ScreenNext(void) {
	if (++menuIndex > menuSize) {
		menuIndex = 0;
		if (wrapMenu)
			displayIndex -= menuSize;
		else displayIndex = 0;
	}
	
	if (MENU_LIST_INDEX == (MENU_LIST_SIZE - 1)) {
		if ((wrapMenu) || (menuIndex < menuSize))
		displayIndex++;
	}
}

void ScreenPrev(void) {
	if (--menuIndex < 0) {
		menuIndex = menuSize;
		if (wrapMenu)
			displayIndex = menuSize;
		else displayIndex = 0;
	}

	if (MENU_LIST_INDEX == 0) {
		if (wrapMenu || menuIndex)
		displayIndex--;
	}
}

void ScreenChange(void) {
	anchorCount = menuIndex = menuSize = displayIndex = wrapMenu = 0;
		
	tempMenuPtr = currentMenuItem;
	
	while ((MENU_VALID(TEMP_MENU_NEXT)) && (TEMP_MENU_NEXT != currentMenuItem)) {
		tempMenuPtr = TEMP_MENU_NEXT;
		if (TEMP_MENU_ENABLED)
			menuSize++;
	}
	
	if (!MENU_VALID(TEMP_MENU_NEXT)) {	// not a wrap around menu!
		tempMenuPtr = currentMenuItem;
		while ((MENU_VALID(TEMP_MENU_PREVIOUS)) && (TEMP_MENU_PREVIOUS != currentMenuItem)) {
			tempMenuPtr = TEMP_MENU_PREVIOUS;
			if (TEMP_MENU_ENABLED) {
				menuSize++;
				if (!MENU_VALID(MENU_ANCHOR))
					menuIndex++;
			}
		}
	}
	else if (menuSize > MENU_LIST_SIZE) {
		wrapMenu = 1;
		displayIndex--;
	}
		
	if (MENU_VALID(MENU_ANCHOR) && (MENU_ANCHOR != currentMenuItem)) {
		tempMenuPtr = currentMenuItem;
							
		while ((MENU_VALID(TEMP_MENU_PREVIOUS)) && (MENU_ANCHOR != tempMenuPtr)) {
			if (TEMP_MENU_ENABLED) {
				ScreenNext();
				anchorCount++;
			}
			tempMenuPtr = TEMP_MENU_PREVIOUS;
		}
	}
}

void Menu_Navigate(enum menuDirection dir, uint16_t time) {
	valueRule_t *rulePtr;
	uint8_t updateScreens = 0;
		
	switch (dir) {
		case MENU_NAV_NONE:
			if (MENU_VALID(MENU_TIMEOUT)) {
				if (MENU_TIMEOUT_TIME && ((menuTimeout - time - 1) > 0xF000)) {
					if (MENU_VALUE != NULL) {						
						rulePtr = MENU_RULE_PTR;
						if ((*rulePtr).liveUpdate) {
							switch ((enum valueType)(MENU_VAL_TYPE)) {
								case TYPE_UINT8:  (*(( uint8_t*)(MENU_VALUE))) = ( uint8_t)((*rulePtr).origVal);		break;
								case TYPE_UINT16: (*((uint16_t*)(MENU_VALUE))) = (uint16_t)((*rulePtr).origVal);		break;
								case TYPE_UINT32: (*((uint32_t*)(MENU_VALUE))) = (uint32_t)((*rulePtr).origVal);		break;
								case TYPE_SINT8:  (*((  int8_t*)(MENU_VALUE))) = (  int8_t)((*rulePtr).origVal);		break;
								case TYPE_SINT16: (*(( int16_t*)(MENU_VALUE))) = ( int16_t)((*rulePtr).origVal);		break;
								case TYPE_SINT32: (*(( int32_t*)(MENU_VALUE))) = ( int32_t)((*rulePtr).origVal);		break;
								case TYPE_SINT8_PERC_1DP:  (*((  int8_t*)(MENU_VALUE))) = (  int8_t)((*rulePtr).origVal);		break;
								case TYPE_YES_NO: (*(( uint8_t*)(MENU_VALUE))) = ( uint8_t)((*rulePtr).origVal);		break;
								default: break;
							}
						}
					}
										
					if (MENU_ON_EXIT != NULL)
						((menuCallback_t)(MENU_ON_EXIT))(Menu_GetCurrentMenu());
					
					currentMenuItem = MENU_TIMEOUT;
					menuTimeout = time + (uint16_t)(MENU_TIMEOUT_TIME);
					updateScreens = 1;
					ScreenChange();
					
					if (MENU_ON_ENTER!= NULL)
						((menuCallback_t)(MENU_ON_ENTER))(Menu_GetCurrentMenu());
				}
			}
			else if MENU_VALID(currentMenuItem) 
				updateScreens = 1;
			break;
		case MENU_NAV_ENTER:
			rulePtr = MENU_RULE_PTR;
			if (MENU_VALUE != NULL) {
				updateScreens = 1;
				
				switch ((enum valueType)(MENU_VAL_TYPE)) {
					case TYPE_UINT8:  (*(( uint8_t*)(MENU_VALUE))) = ( uint8_t)((*rulePtr).currVal);		break;
					case TYPE_UINT16: (*((uint16_t*)(MENU_VALUE))) = (uint16_t)((*rulePtr).currVal);		break;
					case TYPE_UINT32: (*((uint32_t*)(MENU_VALUE))) = (uint32_t)((*rulePtr).currVal);		break;
					case TYPE_SINT8:  (*((  int8_t*)(MENU_VALUE))) = (  int8_t)((*rulePtr).currVal);		break;
					case TYPE_SINT16: (*(( int16_t*)(MENU_VALUE))) = ( int16_t)((*rulePtr).currVal);		break;
					case TYPE_SINT32: (*(( int32_t*)(MENU_VALUE))) = ( int32_t)((*rulePtr).currVal);		break;
					case TYPE_SINT8_PERC_1DP:  (*((  int8_t*)(MENU_VALUE))) = (  int8_t)((*rulePtr).currVal);		break;
					case TYPE_YES_NO: (*(( uint8_t*)(MENU_VALUE))) = ( uint8_t)((*rulePtr).currVal);		break;
					default: break;
				}
				
				(*rulePtr).origVal = (*rulePtr).currVal;
				
				if (MENU_ON_SAVE != NULL)
					((menuCallback_t)(MENU_ON_SAVE))(Menu_GetCurrentMenu());
			}
			if (MENU_VALID(MENU_CHILD)) {
				updateScreens = 1;
				
				if (MENU_ON_SAVE != NULL)
					((menuCallback_t)(MENU_ON_SAVE))(Menu_GetCurrentMenu());
				if (MENU_ON_EXIT != NULL)
					((menuCallback_t)(MENU_ON_EXIT))(Menu_GetCurrentMenu());
				
				
				oldMenuPtr = currentMenuItem;
				
				currentMenuItem = MENU_CHILD;
				
				while ((!MENU_ENABLED) && (MENU_VALID(MENU_NEXT))) {
					currentMenuItem = MENU_NEXT;
				}
				
				while ((!MENU_ENABLED) && (MENU_VALID(MENU_PREVIOUS))) {
					currentMenuItem = MENU_PREVIOUS;
				}
				
				if (!MENU_ENABLED)
					currentMenuItem = oldMenuPtr;
				else ScreenChange();
				
				if (MENU_VALUE != NULL) {
					rulePtr = MENU_RULE_PTR;
					if ((*rulePtr).loadRealVal) {
						switch ((enum valueType)(MENU_VAL_TYPE)) {
							case TYPE_UINT8:  (*rulePtr).currVal = (*(( uint8_t*)(MENU_VALUE)));		break;
							case TYPE_UINT16: (*rulePtr).currVal = (*((uint16_t*)(MENU_VALUE)));		break;
							case TYPE_UINT32: (*rulePtr).currVal = (*((uint32_t*)(MENU_VALUE)));		break;
							case TYPE_SINT8:  (*rulePtr).currVal = (*((  int8_t*)(MENU_VALUE)));		break;
							case TYPE_SINT16: (*rulePtr).currVal = (*(( int16_t*)(MENU_VALUE)));		break;
							case TYPE_SINT32: (*rulePtr).currVal = (*(( int32_t*)(MENU_VALUE)));		break;
							case TYPE_SINT8_PERC_1DP:  (*rulePtr).currVal = (*((  int8_t*)(MENU_VALUE)));		break;
							case TYPE_YES_NO: (*rulePtr).currVal = (*(( uint8_t*)(MENU_VALUE)));		break;
							default: break;
						}
						
						if ((*rulePtr).currVal > (*rulePtr).maxVal)
							(*rulePtr).currVal = (*rulePtr).maxVal;
						if ((*rulePtr).currVal < (*rulePtr).minVal)
							(*rulePtr).currVal = (*rulePtr).minVal;
						
						(*rulePtr).origVal = (*rulePtr).currVal;
					}
					else (*rulePtr).currVal = (*rulePtr).defVal;
				}
				
				if (MENU_ON_ENTER!= NULL)
					((menuCallback_t)(MENU_ON_ENTER))(Menu_GetCurrentMenu());
			}
			break;
		
		case MENU_NAV_EXIT:
			if (MENU_VALUE != NULL) {
				updateScreens = 1;
				rulePtr = MENU_RULE_PTR;
				if ((*rulePtr).liveUpdate) {
					switch ((enum valueType)(MENU_VAL_TYPE)) {
						case TYPE_UINT8:  (*(( uint8_t*)(MENU_VALUE))) = ( uint8_t)((*rulePtr).origVal);		break;
						case TYPE_UINT16: (*((uint16_t*)(MENU_VALUE))) = (uint16_t)((*rulePtr).origVal);		break;
						case TYPE_UINT32: (*((uint32_t*)(MENU_VALUE))) = (uint32_t)((*rulePtr).origVal);		break;
						case TYPE_SINT8:  (*((  int8_t*)(MENU_VALUE))) = (  int8_t)((*rulePtr).origVal);		break;
						case TYPE_SINT16: (*(( int16_t*)(MENU_VALUE))) = ( int16_t)((*rulePtr).origVal);		break;
						case TYPE_SINT32: (*(( int32_t*)(MENU_VALUE))) = ( int32_t)((*rulePtr).origVal);		break;
						case TYPE_SINT8_PERC_1DP:  (*((  int8_t*)(MENU_VALUE))) = (  int8_t)((*rulePtr).origVal);		break;
						case TYPE_YES_NO: (*(( uint8_t*)(MENU_VALUE))) = ( uint8_t)((*rulePtr).origVal);		break;
						default: break;
					}
				}
			}
			
			if (MENU_VALID(MENU_PARENT)) {
				updateScreens = 1;
				
				if (MENU_ON_EXIT != NULL)
					((menuCallback_t)(MENU_ON_EXIT))(Menu_GetCurrentMenu());
										
				oldMenuPtr = currentMenuItem;
					
				do {
					currentMenuItem = MENU_PARENT;
						
					while ((!MENU_ENABLED) && (MENU_VALID(MENU_NEXT))) {
						currentMenuItem = MENU_NEXT;
					}
						
					while ((!MENU_ENABLED) && (MENU_VALID(MENU_PREVIOUS))) {
						currentMenuItem = MENU_PREVIOUS;
					}
				} while ((!MENU_ENABLED) && (MENU_VALID(MENU_PARENT)));
				
				if (!MENU_ENABLED)
					currentMenuItem = oldMenuPtr;
				else ScreenChange();
				
				if (MENU_ON_ENTER != NULL)
					((menuCallback_t)(MENU_ON_ENTER))(Menu_GetCurrentMenu());
			}
			break;
		
		case MENU_NAV_UP:
			if (MENU_VALID(MENU_PREVIOUS)) {
				updateScreens = 1;
								
				oldMenuPtr = currentMenuItem;
				
				do {
					currentMenuItem = MENU_PREVIOUS;
				} while ((!MENU_ENABLED) && MENU_VALID(MENU_PREVIOUS));
				
				if (!MENU_ENABLED)
					currentMenuItem = oldMenuPtr;
				else ScreenPrev();
			}
			else if (MENU_VALUE != NULL) {
				updateScreens = 1;
				
				rulePtr = MENU_RULE_PTR;
				(*rulePtr).currVal++;
				
				if ((*rulePtr).currVal > (*rulePtr).maxVal) {
					if ((*rulePtr).wrapAround)
						(*rulePtr).currVal = (*rulePtr).minVal;
					else
						(*rulePtr).currVal = (*rulePtr).maxVal;
				}
				
				switch ((enum valueType)(MENU_VAL_TYPE)) {
					case TYPE_UINT8:           if ((*rulePtr).liveUpdate)	*(( uint8_t*)(MENU_VALUE)) = (*rulePtr).currVal;		break;
					case TYPE_UINT16:          if ((*rulePtr).liveUpdate)	*((uint16_t*)(MENU_VALUE)) = (*rulePtr).currVal;		break;
					case TYPE_UINT32:          if ((*rulePtr).liveUpdate)	*((uint32_t*)(MENU_VALUE)) = (*rulePtr).currVal;		break;
					case TYPE_SINT8:           if ((*rulePtr).liveUpdate)	*((  int8_t*)(MENU_VALUE)) = (*rulePtr).currVal;		break;
					case TYPE_SINT16:          if ((*rulePtr).liveUpdate)	*(( int16_t*)(MENU_VALUE)) = (*rulePtr).currVal;		break;
					case TYPE_SINT32:          if ((*rulePtr).liveUpdate)	*(( int32_t*)(MENU_VALUE)) = (*rulePtr).currVal;		break;
					case TYPE_SINT8_PERC_1DP:  if ((*rulePtr).liveUpdate)	*((  int8_t*)(MENU_VALUE)) = (*rulePtr).currVal;		break;
					case TYPE_YES_NO:          if ((*rulePtr).liveUpdate)	*(( uint8_t*)(MENU_VALUE)) = (*rulePtr).currVal;		break;
					default: break;
				}
				
				if (MENU_ON_CHANGE!= NULL)
					((menuCallback_t)(MENU_ON_CHANGE))(Menu_GetCurrentMenu());
			}
			break;
		
		case MENU_NAV_DN:
			if (MENU_VALID(MENU_NEXT)) {
				updateScreens = 1;
				
				oldMenuPtr = currentMenuItem;
				
				do {
					currentMenuItem = MENU_NEXT;
				} while ((!MENU_ENABLED) && MENU_VALID(MENU_NEXT));
				
				if (!MENU_ENABLED)
					currentMenuItem = oldMenuPtr;
				else ScreenNext();
			}
			else if (MENU_VALUE != NULL) {
				updateScreens = 1;
				
				rulePtr = MENU_RULE_PTR;
				(*rulePtr).currVal--;
				
				if ((*rulePtr).currVal < (*rulePtr).minVal) {
					if ((*rulePtr).wrapAround)
						(*rulePtr).currVal = (*rulePtr).maxVal;
					else
						(*rulePtr).currVal = (*rulePtr).minVal;
				}
				
				switch ((enum valueType)(MENU_VAL_TYPE)) {
					case TYPE_UINT8:           if ((*rulePtr).liveUpdate)	*(( uint8_t*)(MENU_VALUE)) = (*rulePtr).currVal;		break;
					case TYPE_UINT16:          if ((*rulePtr).liveUpdate)	*((uint16_t*)(MENU_VALUE)) = (*rulePtr).currVal;		break;
					case TYPE_UINT32:          if ((*rulePtr).liveUpdate)	*((uint32_t*)(MENU_VALUE)) = (*rulePtr).currVal;		break;
					case TYPE_SINT8:           if ((*rulePtr).liveUpdate)	*((  int8_t*)(MENU_VALUE)) = (*rulePtr).currVal;		break;
					case TYPE_SINT16:          if ((*rulePtr).liveUpdate)	*(( int16_t*)(MENU_VALUE)) = (*rulePtr).currVal;		break;
					case TYPE_SINT32:          if ((*rulePtr).liveUpdate)	*(( int32_t*)(MENU_VALUE)) = (*rulePtr).currVal;		break;
					case TYPE_SINT8_PERC_1DP:  if ((*rulePtr).liveUpdate)	*((  int8_t*)(MENU_VALUE)) = (*rulePtr).currVal;		break;
					case TYPE_YES_NO:          if ((*rulePtr).liveUpdate)	*(( uint8_t*)(MENU_VALUE)) = (*rulePtr).currVal;		break;
					default: break;
				}
				
				if (MENU_ON_CHANGE!= NULL)
					((menuCallback_t)(MENU_ON_CHANGE))(Menu_GetCurrentMenu());
			}
			break;
		
		};
	
	if (dir != MENU_NAV_NONE)
		menuTimeout = time + (uint16_t)(MENU_TIMEOUT_TIME);
	
	if (updateScreens) {
		if (MENU_ON_DISPLAY != NULL)
			((menuCallback_t)(MENU_ON_DISPLAY))(Menu_GetCurrentMenu());
		else UpdateMenuScreens();
	}
}
