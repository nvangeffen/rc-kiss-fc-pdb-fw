/*
 * MenuConfig.h
 *
 * Created: 16/07/2016 7:20:42 p.m.
 *  Author: Ned
 */ 


#ifndef MENUCONFIG_H_
#define MENUCONFIG_H_

#define STORE_MENU_IN_FLASH

#define MENU_ROWS		8
#define MENU_COLS		20

#define MENU_TITLE		1

#endif /* MENUCONFIG_H_ */