/*
 * MenuLayout.h
 *
 * Created: 16/07/2016 8:34:20 p.m.
 *  Author: Ned
 */ 


#ifndef MENULAYOUT_H_
#define MENULAYOUT_H_

#include "..\Memory\GlobalData.h"
#include "..\WS LED\WS2812.h"

void OpenMenu(const struct menuItem_t * menu);
void LoadDefaults(const struct menuItem_t * menu);
void DisplayVtxData(const struct menuItem_t *menu);
void DisplayRev(const struct menuItem_t *menu);
void DisplayLogo(const struct menuItem_t *menu);
void CloseMenu(const struct menuItem_t *);
void SetLedModeVTX(const struct menuItem_t * menu);
void SetLedModePreset(const struct menuItem_t * menu);
void SetLedModeUser(const struct menuItem_t * menu);
void SetLedModeChannel(const struct menuItem_t * menu);
void UpdateLED(const struct menuItem_t *);
void DisplayTime(const struct menuItem_t *);
void DisplayLedRed(const struct menuItem_t *);
void DisplayLedGrn(const struct menuItem_t *);
void DisplayLedBlu(const struct menuItem_t *);
void DisplaySaveScreen(const struct menuItem_t *);
void DisplayPresetColour(const struct menuItem_t *menu);
void SetRssiSourceADC(const struct menuItem_t * menu);
void SetRssiSourceRC(const struct menuItem_t * menu);
void SetRssiMax(const struct menuItem_t * menu);
void DisplayRssiCalibrate(const struct menuItem_t *menu);
void SetVoltSourceFC(const struct menuItem_t * menu);
void SetVoltSourceESC(const struct menuItem_t * menu);
void SetVoltSourceADC(const struct menuItem_t * menu);
void DisplayAccGyro(const struct menuItem_t *menu);
void DisplayBand(const struct menuItem_t *menu);
void DisplayOutputPower(const struct menuItem_t *menu);
void SetVtxPowerModeAuto(const struct menuItem_t * menu);
void SetVtxPowerModeOn(const struct menuItem_t * menu);
void SetVtxPowerModePitt(const struct menuItem_t * menu);
void SetVtxPowerModeRc(const struct menuItem_t * menu);
void SetVtxPowerModeRManual(const struct menuItem_t * menu);
void SetupVTX(const struct menuItem_t *menu);
void SetVtxBandUser(const struct menuItem_t * menu);
void DisplayPitMode(const struct menuItem_t * menu);

MENU_RULE(rgbLED, 1, 1, 1, 0x00, 0xFF, 0);
MENU_RULE(brightRule, 1, 0, 1, 0, 100, 0);
MENU_RULE(presetRule, 1, 1, 1, 0, COL_MAX_COLOURS - 1, 0);
MENU_RULE(rcChanRule, 1, 1, 1, 0, 3, 0);
MENU_RULE(vCalibRule, 1, 0, 1, -99, +99, 0);
MENU_RULE(aCalibRule, 1, 0, 1, -99, +99, 0);

MENU_RULE(vtxBandRule, 0, 1, 1, 0, BAND_MAX_BANDS - 2, 0);
MENU_RULE(vtxChanRule, 0, 1, 1, 1, 8, 0);
MENU_RULE(vtxPowerRule, 0, 1, 1, 0, VTX_MAX_LEVELS - 1, 0);
MENU_RULE(vtxPitModeRule, 0, 1, 1, 0, VTX_PIT_MAX - 1, 0);
MENU_RULE(vtxFreqRule, 1, 0, 1, VTX_MIN_FREQ, VTX_MAX_FREQ, 0);

MENU_RULE(osdShowRule, 1, 1, 1, 0, 1, 0);

//        Menu Name         Next Menu     Previous Menu Parent Menu    Child Menu        Anchor Menu  Enabled uint8_t     Timeout Menu   Time     Variable Rule     Variable               Value Type             Enter Func       Exit Func    Save Func            Change Func  Display Func            Menu Text
MENU_ITEM(Menu_Close,       NULL_MENU,    NULL_MENU,    NULL_MENU,     NULL_MENU,        NULL_MENU,   ENABLED_MENU,       NULL_MENU,     15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          CloseMenu,       NULL,        NULL,                NULL,        NULL,                   "MENU Close");
MENU_ITEM(Menu_Logo,        Menu_Rev,     Menu_VTX,     Menu_Close,    Menu_1,           NULL_MENU,   ENABLED_MENU,       Menu_Close,    5000,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        DisplayLogo,            "");
MENU_ITEM(Menu_Rev,         Menu_Sens,    Menu_Logo,    Menu_Close,    Menu_1,           NULL_MENU,   ENABLED_MENU,       Menu_Close,    5000,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        DisplayRev,             "");
MENU_ITEM(Menu_Sens,        Menu_VTX,     Menu_Rev,     Menu_Close,    Menu_1,           NULL_MENU,   ENABLED_MENU,       NULL_MENU,     15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        DisplayAccGyro,         "");
MENU_ITEM(Menu_VTX,         Menu_Logo,    Menu_Sens,    Menu_Close,    Menu_1,           NULL_MENU,   ENABLED_MENU,       Menu_Close,    5000,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        DisplayVtxData,         "");

//        Menu Name         Next Menu     Previous Menu Parent Menu    Child Menu        Anchor Menu  Enabled uint8_t     Timeout Menu   Time     Variable Rule     Variable               Value Type             Enter Func       Exit Func    Save Func            Change Func  Display Func            Menu Text
MENU_ITEM(Menu_1,           Menu_2,       NULL_MENU,    Menu_Close,    Menu_1_1,         Menu_1,      ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "RGB LED");
MENU_ITEM(Menu_2,           Menu_3,       Menu_1,       Menu_Close,    Menu_2_1,         Menu_1,      ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "RSSI");
MENU_ITEM(Menu_3,           Menu_4,       Menu_2,       Menu_Close,    Menu_3_1,         Menu_1,      ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Voltage");
MENU_ITEM(Menu_4,           Menu_5,       Menu_3,       Menu_Close,    Menu_4_1,         Menu_1,      vtxData.seenIt,     Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "VTX Settings");
MENU_ITEM(Menu_5,           Menu_6,       Menu_4,       Menu_Close,    Menu_5_1,         Menu_1,      ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "OSD Options");
MENU_ITEM(Menu_6,           Menu_7,       Menu_5,       Menu_Close,    Menu_6_1,         Menu_1,      ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        LoadDefaults,        NULL,        NULL,                   "Load Defaults");
MENU_ITEM(Menu_7,           NULL_MENU,    Menu_6,       Menu_Close,    Menu_Close,       Menu_1,      ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Close Menu");

// Defaults
//        Menu Name         Next Menu     Previous Menu Parent Menu    Child Menu        Anchor Menu  Enabled uint8_t     Timeout Menu   Time     Variable Rule     Variable               Value Type             Enter Func       Exit Func    Save Func            Change Func  Display Func            Menu Text
MENU_ITEM(Menu_6_1,         NULL_MENU,    NULL_MENU,    Menu_6,        Menu_6_1_S,       NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Are you sure?");
MENU_ITEM(Menu_6_1_S,       NULL_MENU,    NULL_MENU,    Menu_6_1,      NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_6,        1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");

// LED
//        Menu Name         Next Menu     Previous Menu Parent Menu    Child Menu        Anchor Menu  Enabled uint8_t     Timeout Menu   Time     Variable Rule     Variable               Value Type             Enter Func       Exit Func    Save Func            Change Func  Display Func            Menu Text
MENU_ITEM(Menu_1_1,         Menu_1_2,     Menu_1_5,     Menu_1,        Menu_1_1_1,       Menu_1_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        SetLedModePreset,    NULL,        NULL,                   "Pick Presets");
MENU_ITEM(Menu_1_2,         Menu_1_3,     Menu_1_1,     Menu_1,        Menu_1_2_1,       Menu_1_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        SetLedModeChannel,   NULL,        NULL,                   "Pick RC Channel");
MENU_ITEM(Menu_1_3,         Menu_1_4,     Menu_1_2,     Menu_1,        Menu_1_3_1,       Menu_1_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        SetLedModeUser,      NULL,        NULL,                   "Pick Any Colour");
MENU_ITEM(Menu_1_4,         Menu_1_5,     Menu_1_3,     Menu_1,        Menu_1_4_S,       Menu_1_1,    vtxData.seenIt,     Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        SetLedModeVTX,       NULL,        NULL,                   "VTX Freq Colour");
MENU_ITEM(Menu_1_5,         Menu_1_1,     Menu_1_4,     Menu_1,        Menu_1_5_1,       Menu_1_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Set Brightness");

MENU_ITEM(Menu_1_3_1,       Menu_1_3_2,   Menu_1_3_3,   Menu_1_3,      Menu_1_3_1_1,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Set Red");
MENU_ITEM(Menu_1_3_2,       Menu_1_3_3,   Menu_1_3_1,   Menu_1_3,      Menu_1_3_2_1,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Set Green");
MENU_ITEM(Menu_1_3_3,       Menu_1_3_1,   Menu_1_3_2,   Menu_1_3,      Menu_1_3_3_1,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Set Blue");

MENU_ITEM(Menu_1_3_1_1,     NULL_MENU,    NULL_MENU,    Menu_1_3_1,    Menu_1_3_1_1_S,   NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   rgbLED,           nvMem.manualCol.r,     TYPE_UINT8,            NULL,            UpdateLED,   NULL,                UpdateLED,   DisplayLedRed,          "");
MENU_ITEM(Menu_1_3_2_1,     NULL_MENU,    NULL_MENU,    Menu_1_3_2,    Menu_1_3_2_1_S,   NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   rgbLED,           nvMem.manualCol.g,     TYPE_UINT8,            NULL,            UpdateLED,   NULL,                UpdateLED,   DisplayLedGrn,          "");
MENU_ITEM(Menu_1_3_3_1,     NULL_MENU,    NULL_MENU,    Menu_1_3_3,    Menu_1_3_3_1_S,   NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   rgbLED,           nvMem.manualCol.b,     TYPE_UINT8,            NULL,            UpdateLED,   NULL,                UpdateLED,   DisplayLedBlu,          "");

MENU_ITEM(Menu_1_3_1_1_S,   NULL_MENU,    NULL_MENU,    Menu_1_3_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_1_3_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_1_3_2_1_S,   NULL_MENU,    NULL_MENU,    Menu_1_3_2,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_1_3_2,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_1_3_3_1_S,   NULL_MENU,    NULL_MENU,    Menu_1_3_3,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_1_3_3,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");

MENU_ITEM(Menu_1_1_1,       NULL_MENU,    NULL_MENU,    Menu_1_1,      Menu_1_1_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   presetRule,       nvMem.userPreset,      TYPE_UINT8,            NULL,            NULL,        NULL,                NULL,        DisplayPresetColour,    "");
MENU_ITEM(Menu_1_2_1,       NULL_MENU,    NULL_MENU,    Menu_1_2,      Menu_1_2_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   rcChanRule,       nvMem.rcColChan,       TYPE_UINT8,            NULL,            NULL,        NULL,                NULL,        NULL,                   "RC Channel");
MENU_ITEM(Menu_1_4_S,       NULL_MENU,    NULL_MENU,    Menu_1_4,      NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_1_4,      1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_1_5_1,       NULL_MENU,    NULL_MENU,    Menu_1_5,      Menu_1_5_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   brightRule,       nvMem.ledBrightness,   TYPE_UINT8,            NULL,            NULL,        NULL,                NULL,        NULL,                   "Brightness");

MENU_ITEM(Menu_1_1_1_S,     NULL_MENU,    NULL_MENU,    Menu_1_1_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_1_1_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_1_2_1_S,     NULL_MENU,    NULL_MENU,    Menu_1_2_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_1_2_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_1_5_1_S,     NULL_MENU,    NULL_MENU,    Menu_1_5_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_1_5_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");

// RSSI
//        Menu Name         Next Menu     Previous Menu Parent Menu    Child Menu        Anchor Menu  Enabled uint8_t     Timeout Menu   Time     Variable Rule     Variable               Value Type             Enter Func       Exit Func    Save Func            Change Func  Display Func            Menu Text
MENU_ITEM(Menu_2_1,         Menu_2_2,     Menu_2_3,     Menu_2,        Menu_2_1_1,       Menu_2_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        SetRssiSourceRC,     NULL,        NULL,                   "RSSI RC Channel");
MENU_ITEM(Menu_2_2,         Menu_2_3,     Menu_2_1,     Menu_2,        Menu_2_2_S,       Menu_2_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        SetRssiSourceADC,    NULL,        NULL,                   "RSSI Analog Read");
MENU_ITEM(Menu_2_3,         Menu_2_1,     Menu_2_2,     Menu_2,        Menu_2_3_1,       Menu_2_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Calibration");

MENU_ITEM(Menu_2_1_1,       NULL_MENU,    NULL_MENU,    Menu_2_1,      Menu_2_1_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   rcChanRule,       nvMem.rssiRcChan,      TYPE_UINT8,            NULL,            NULL,        NULL,                NULL,        NULL,                   "RC Channel");
MENU_ITEM(Menu_2_2_S,       NULL_MENU,    NULL_MENU,    Menu_2_2,      NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_2_2,      1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_2_3_1,       NULL_MENU,    NULL_MENU,    Menu_2_3,      Menu_2_3_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        SetRssiMax,          NULL,        DisplayRssiCalibrate,   "");

MENU_ITEM(Menu_2_1_1_S,     NULL_MENU,    NULL_MENU,    Menu_2_1_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_2_1_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_2_3_1_S,     NULL_MENU,    NULL_MENU,    Menu_2_3_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_2_3_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");

// Voltage/current
//        Menu Name         Next Menu     Previous Menu Parent Menu    Child Menu        Anchor Menu  Enabled uint8_t     Timeout Menu   Time     Variable Rule     Variable               Value Type             Enter Func       Exit Func    Save Func            Change Func  Display Func            Menu Text
MENU_ITEM(Menu_3_1,         Menu_3_2,     Menu_3_5,     Menu_3,        Menu_3_1_1,       Menu_3_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        SetVoltSourceADC,    NULL,        NULL,                   "Source ADC");
MENU_ITEM(Menu_3_2,         Menu_3_3,     Menu_3_1,     Menu_3,        Menu_3_2_1,       Menu_3_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        SetVoltSourceFC,     NULL,        NULL,                   "Source FC");
MENU_ITEM(Menu_3_3,         Menu_3_4,     Menu_3_2,     Menu_3,        Menu_3_3_1,       Menu_3_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        SetVoltSourceESC,    NULL,        NULL,                   "Source ESC");
MENU_ITEM(Menu_3_4,         Menu_3_5,     Menu_3_3,     Menu_3,        Menu_3_4_1,       Menu_3_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Calibrate A");
MENU_ITEM(Menu_3_5,         Menu_3_1,     Menu_3_4,     Menu_3,        Menu_3_5_1,       Menu_3_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Calibrate mAh");

MENU_ITEM(Menu_3_1_1,       NULL_MENU,    NULL_MENU,    Menu_3_1,      Menu_3_1_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   vCalibRule,       nvMem.tuneAdcV,        TYPE_SINT8_PERC_1DP,   NULL,            NULL,        NULL,                NULL,        NULL,                   "Calibrate");
MENU_ITEM(Menu_3_2_1,       NULL_MENU,    NULL_MENU,    Menu_3_2,      Menu_3_2_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   vCalibRule,       nvMem.tuneFcV,         TYPE_SINT8_PERC_1DP,   NULL,            NULL,        NULL,                NULL,        NULL,                   "Calibrate");
MENU_ITEM(Menu_3_3_1,       NULL_MENU,    NULL_MENU,    Menu_3_3,      Menu_3_3_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   vCalibRule,       nvMem.tuneEscV,        TYPE_SINT8_PERC_1DP,   NULL,            NULL,        NULL,                NULL,        NULL,                   "Calibrate");
MENU_ITEM(Menu_3_4_1,       NULL_MENU,    NULL_MENU,    Menu_3_4,      Menu_3_4_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   aCalibRule,       nvMem.tuneA,           TYPE_SINT8_PERC_1DP,   NULL,            NULL,        NULL,                NULL,        NULL,                   "Calibrate");
MENU_ITEM(Menu_3_5_1,       NULL_MENU,    NULL_MENU,    Menu_3_5,      Menu_3_5_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   aCalibRule,       nvMem.tuneMah,         TYPE_SINT8_PERC_1DP,   NULL,            NULL,        NULL,                NULL,        NULL,                   "Calibrate");

MENU_ITEM(Menu_3_1_1_S,     NULL_MENU,    NULL_MENU,    Menu_3_1_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_3_1_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_3_2_1_S,     NULL_MENU,    NULL_MENU,    Menu_3_2_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_3_2_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_3_3_1_S,     NULL_MENU,    NULL_MENU,    Menu_3_3_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_3_3_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_3_4_1_S,     NULL_MENU,    NULL_MENU,    Menu_3_4_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_3_4_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_3_5_1_S,     NULL_MENU,    NULL_MENU,    Menu_3_5_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_3_5_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");

// VTX
//        Menu Name         Next Menu     Previous Menu Parent Menu    Child Menu        Anchor Menu  Enabled uint8_t     Timeout Menu   Time     Variable Rule     Variable               Value Type             Enter Func       Exit Func    Save Func            Change Func  Display Func            Menu Text
MENU_ITEM(Menu_4_1,         Menu_4_2,     Menu_4_5,     Menu_4,        Menu_4_1_1,       Menu_4_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Band");
MENU_ITEM(Menu_4_2,         Menu_4_3,     Menu_4_1,     Menu_4,        Menu_4_2_1,       Menu_4_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Channel");
MENU_ITEM(Menu_4_3,         Menu_4_4,     Menu_4_2,     Menu_4,        Menu_4_3_1,       Menu_4_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Output Power");
MENU_ITEM(Menu_4_4,         Menu_4_5,     Menu_4_3,     Menu_4,        Menu_4_4_1,       Menu_4_1,    vtxData.revision,   Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Pit Mode");
MENU_ITEM(Menu_4_5,         Menu_4_1,     Menu_4_4,     Menu_4,        Menu_4_5_1,       Menu_4_1,    DISABLED_MENU,      Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "User Freq");

MENU_ITEM(Menu_4_1_1,       NULL_MENU,    NULL_MENU,    Menu_4_1,      Menu_4_1_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   vtxBandRule,      nvMem.vtxBand,         TYPE_UINT8,            NULL,            NULL,        NULL,                NULL,        DisplayBand,            "");
MENU_ITEM(Menu_4_2_1,       NULL_MENU,    NULL_MENU,    Menu_4_2,      Menu_4_2_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   vtxChanRule,      nvMem.vtxChannel,      TYPE_UINT8,            NULL,            NULL,        NULL,                NULL,        NULL,                   "Channel");
MENU_ITEM(Menu_4_3_1,       NULL_MENU,    NULL_MENU,    Menu_4_3,      Menu_4_3_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   vtxPowerRule,     nvMem.vtxPower,        TYPE_UINT8,            NULL,            NULL,        NULL,                NULL,        DisplayOutputPower,     "Output Power");
MENU_ITEM(Menu_4_4_1,       NULL_MENU,    NULL_MENU,    Menu_4_4,      Menu_4_4_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   vtxPitModeRule,   nvMem.vtxPitMode,      TYPE_UINT8,            NULL,            NULL,        NULL,                NULL,        DisplayPitMode,         "Pit Mode");
MENU_ITEM(Menu_4_5_1,       NULL_MENU,    NULL_MENU,    Menu_4_5,      Menu_4_5_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   vtxFreqRule,      nvMem.vtxFrequency,    TYPE_UINT16,           NULL,            NULL,        SetVtxBandUser,      NULL,        NULL,                   "Frequency");

MENU_ITEM(Menu_4_1_1_S,     NULL_MENU,    NULL_MENU,    Menu_4_1_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_4_1_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   SetupVTX,    NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_4_2_1_S,     NULL_MENU,    NULL_MENU,    Menu_4_2_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_4_2_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   SetupVTX,    NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_4_3_1_S,     NULL_MENU,    NULL_MENU,    Menu_4_3_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_4_3_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   SetupVTX,    NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_4_4_1_S,     NULL_MENU,    NULL_MENU,    Menu_4_4_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_4_4_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   SetupVTX,    NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_4_5_1_S,     NULL_MENU,    NULL_MENU,    Menu_4_5_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_4_5_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   SetupVTX,    NULL,                NULL,        DisplaySaveScreen,      "");

// OSD Options
//        Menu Name         Next Menu     Previous Menu Parent Menu    Child Menu        Anchor Menu  Enabled uint8_t     Timeout Menu   Time     Variable Rule     Variable               Value Type             Enter Func       Exit Func    Save Func            Change Func  Display Func            Menu Text
MENU_ITEM(Menu_5_1,         Menu_5_2,     Menu_5_10,    Menu_5,        Menu_5_1_1,       Menu_5_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Show Amps");
MENU_ITEM(Menu_5_2,         Menu_5_3,     Menu_5_1,     Menu_5,        Menu_5_2_1,       Menu_5_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Show Volts");
MENU_ITEM(Menu_5_3,         Menu_5_4,     Menu_5_2,     Menu_5,        Menu_5_3_1,       Menu_5_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Show Mah");
MENU_ITEM(Menu_5_4,         Menu_5_5,     Menu_5_3,     Menu_5,        Menu_5_4_1,       Menu_5_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Show ESC Temp");
MENU_ITEM(Menu_5_5,         Menu_5_6,     Menu_5_4,     Menu_5,        Menu_5_5_1,       Menu_5_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Show Timer");
MENU_ITEM(Menu_5_6,         Menu_5_7,     Menu_5_5,     Menu_5,        Menu_5_6_1,       Menu_5_1,    ENABLED_MENU,       Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Show RSSI");
MENU_ITEM(Menu_5_7,         Menu_5_8,     Menu_5_6,     Menu_5,        Menu_5_7_1,       Menu_5_1,    vtxData.seenIt,     Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Show Frequency");
MENU_ITEM(Menu_5_8,         Menu_5_9,     Menu_5_7,     Menu_5,        Menu_5_8_1,       Menu_5_1,    vtxData.seenIt,     Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Show Band");
MENU_ITEM(Menu_5_9,         Menu_5_10,    Menu_5_8,     Menu_5,        Menu_5_9_1,       Menu_5_1,    vtxData.seenIt,     Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Show Power");
MENU_ITEM(Menu_5_10,        Menu_5_1,     Menu_5_9,     Menu_5,        Menu_5_10_1,      Menu_5_1,    vtxData.seenIt,     Menu_Close,    15000,   NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          NULL,            NULL,        NULL,                NULL,        NULL,                   "Show Pit Mode");

MENU_ITEM(Menu_5_1_1,       NULL_MENU,    NULL_MENU,    Menu_5_1,      Menu_5_1_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   osdShowRule,      nvMem.showA,           TYPE_YES_NO,           NULL,            NULL,        NULL,                NULL,        NULL,                   "Show Amps");
MENU_ITEM(Menu_5_2_1,       NULL_MENU,    NULL_MENU,    Menu_5_2,      Menu_5_2_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   osdShowRule,      nvMem.showV,           TYPE_YES_NO,           NULL,            NULL,        NULL,                NULL,        NULL,                   "Show Volts");
MENU_ITEM(Menu_5_3_1,       NULL_MENU,    NULL_MENU,    Menu_5_3,      Menu_5_3_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   osdShowRule,      nvMem.showMah,         TYPE_YES_NO,           NULL,            NULL,        NULL,                NULL,        NULL,                   "Show Mah");
MENU_ITEM(Menu_5_4_1,       NULL_MENU,    NULL_MENU,    Menu_5_4,      Menu_5_4_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   osdShowRule,      nvMem.showTemp,        TYPE_YES_NO,           NULL,            NULL,        NULL,                NULL,        NULL,                   "Show ESC Temp");
MENU_ITEM(Menu_5_5_1,       NULL_MENU,    NULL_MENU,    Menu_5_5,      Menu_5_5_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   osdShowRule,      nvMem.showTime,        TYPE_YES_NO,           NULL,            NULL,        NULL,                NULL,        NULL,                   "Show Timer");
MENU_ITEM(Menu_5_6_1,       NULL_MENU,    NULL_MENU,    Menu_5_6,      Menu_5_6_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   osdShowRule,      nvMem.showRssi,        TYPE_YES_NO,           NULL,            NULL,        NULL,                NULL,        NULL,                   "Show RSSI");
MENU_ITEM(Menu_5_7_1,       NULL_MENU,    NULL_MENU,    Menu_5_7,      Menu_5_7_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   osdShowRule,      nvMem.showFreq,        TYPE_YES_NO,           NULL,            NULL,        NULL,                NULL,        NULL,                   "Show Frequency");
MENU_ITEM(Menu_5_8_1,       NULL_MENU,    NULL_MENU,    Menu_5_8,      Menu_5_8_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   osdShowRule,      nvMem.showBand,        TYPE_YES_NO,           NULL,            NULL,        NULL,                NULL,        NULL,                   "Show Band");
MENU_ITEM(Menu_5_9_1,       NULL_MENU,    NULL_MENU,    Menu_5_9,      Menu_5_9_1_S,     NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   osdShowRule,      nvMem.showPower,       TYPE_YES_NO,           NULL,            NULL,        NULL,                NULL,        NULL,                   "Show Power");
MENU_ITEM(Menu_5_10_1,      NULL_MENU,    NULL_MENU,    Menu_5_10,     Menu_5_10_1_S,    NULL_MENU,   ENABLED_MENU,       Menu_Close,    15000,   osdShowRule,      nvMem.showPitMode,     TYPE_YES_NO,           NULL,            NULL,        NULL,                NULL,        NULL,                   "Show Pit Mode");

MENU_ITEM(Menu_5_1_1_S,     NULL_MENU,    NULL_MENU,    Menu_5_1_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_5_1_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_5_2_1_S,     NULL_MENU,    NULL_MENU,    Menu_5_2_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_5_2_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_5_3_1_S,     NULL_MENU,    NULL_MENU,    Menu_5_3_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_5_3_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_5_4_1_S,     NULL_MENU,    NULL_MENU,    Menu_5_4_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_5_4_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_5_5_1_S,     NULL_MENU,    NULL_MENU,    Menu_5_5_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_5_5_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_5_6_1_S,     NULL_MENU,    NULL_MENU,    Menu_5_6_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_5_6_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_5_7_1_S,     NULL_MENU,    NULL_MENU,    Menu_5_7_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_5_7_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_5_8_1_S,     NULL_MENU,    NULL_MENU,    Menu_5_8_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_5_8_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_5_9_1_S,     NULL_MENU,    NULL_MENU,    Menu_5_9_1,    NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_5_9_1,    1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");
MENU_ITEM(Menu_5_10_1_S,    NULL_MENU,    NULL_MENU,    Menu_5_10_1,   NULL_MENU,        NULL_MENU,   ENABLED_MENU,       Menu_5_10_1,   1500,    NULL_RULE,        NULL_VALUE,            TYPE_UNKNOWN,          DataNvMemSave,   NULL,        NULL,                NULL,        DisplaySaveScreen,      "");

#endif /* MENULAYOUT_H_ */