/*
 * MenuUserCallbacks.c
 *
 * Created: 17/07/2016 5:17:13 p.m.
 *  Author: Ned
 */ 

#include <stdio.h>
#include <avr/pgmspace.h>
#include "MenuSystem.h"
#include "..\Header Files\includes.h"

extern uint16_t secCounter, minCounter, hourCounter;
extern valueRule_t rgbLED;

void LoadDefaults(const struct menuItem_t * menu) {
	DataNvMemDefaults();
}

void OpenMenu(const struct menuItem_t * menu) {
	vtxDetected = vtxData.connected;
	vtxV2Detected = vtxData.revision;
}

void CloseMenu(const struct menuItem_t * menu) {
	Menu_Init(&NULL_MENU, 0);
	DataNvMemSave(menu);
}

void SetLedModePreset(const struct menuItem_t * menu) {
	nvMem.rgbSource = LED_PRESET;
}

void SetLedModeUser(const struct menuItem_t * menu) {
	nvMem.rgbSource = LED_USER;
}

void SetLedModeVTX(const struct menuItem_t * menu) {
	nvMem.rgbSource = LED_VTX;
}

void SetLedModeChannel(const struct menuItem_t * menu) {
	nvMem.rgbSource = LED_CHANNEL;
}

void SetRssiSourceADC(const struct menuItem_t * menu) {
	nvMem.rssiSource = RSSI_ADC;
}

void SetRssiSourceRC(const struct menuItem_t * menu) {
	nvMem.rssiSource = RSSI_RC;
}

void SetVoltSourceFC(const struct menuItem_t * menu) {
	nvMem.vSource = VOLT_FC;
}

void SetVoltSourceESC(const struct menuItem_t * menu) {
	nvMem.vSource = VOLT_ESC;
}

void SetVoltSourceADC(const struct menuItem_t * menu) {
	nvMem.vSource = VOLT_ADC;
}

void SetRssiMax(const struct menuItem_t * menu) {
	switch (nvMem.rssiSource) {
		case RSSI_ADC:	nvMem.rssiMaxAdc = rssi.avg;								break;
		case RSSI_RC:	nvMem.rssiMaxRc  = data.packet.aux[nvMem.rssiRcChan];	break;
		default:
			nvMem.rssiSource = RSSI_RC;
			nvMem.rssiMaxRc = 1000;
	}
}

void SetVtxBandUser(const struct menuItem_t * menu) {
	nvMem.vtxBand = BAND_USER;
}

// Example/test function

void DisplayLogo(const struct menuItem_t *menu) {
	Menu_Clear();
	
	snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR("  %c%c%c%c%c%c%c%c%c%c%c%c      "), 0xC6, 0xC7, 0xC8, 0xC9, 0xCa, 0xCb, 0xCc, 0xCd, 0xCe, 0xCf, 0xD0, 0xD1);
	snprintf_P(&menuScreen[3][0],MENU_COLS,PSTR("  %c%c%c%c%c%c%c%c%c%c%c%c      "), 0xD5, 0xD6, 0xD7, 0xD8, 0xD9, 0xDa, 0xDb, 0xDc, 0xDd, 0xDe, 0xDf, 0xE0);
	snprintf_P(&menuScreen[4][0],MENU_COLS,PSTR("  %c%c%c%c%c%c%c%c%c%c%c%c      "), 0xE4, 0xE5, 0xE6, 0xE7, 0xE8, 0xE9, 0xEa, 0xEb, 0xEc, 0xEd, 0xEe, 0xEf);
	snprintf_P(&menuScreen[5][0],MENU_COLS,PSTR("  %c%c%c%c%c%c%c%c%c%c%c%c      "), 0xF3, 0xF4, 0xF5, 0xF6, 0xF7, 0xF8, 0xF9, 0xFa, 0xFb, 0xFc, 0xFd, 0xFe);
}

void DisplayRev(const struct menuItem_t *menu) {
	Menu_Clear();
	
	snprintf_P(&menuScreen[1][0],MENU_COLS,PSTR("King of Kings      "));
	snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR("KISS FC OSD - PDB  "));
	snprintf_P(&menuScreen[3][0],MENU_COLS,PSTR("Sw Rev :: %2d.%02d.%02d   "), REV_0, REV_1, REV_2);
	snprintf_P(&menuScreen[4][0],MENU_COLS,PSTR("%s"), __DATE__);
}

void DisplayVtxData(const struct menuItem_t *menu) {
	uint16_t frequency;
	uint16_t channel;
	uint16_t txPower;
	char bandStr[8];
	Menu_Clear();

	switch (vtxData.channel / 8) {
		case BAND_A:    sprintf(bandStr,"Band A"); break;
		case BAND_B:    sprintf(bandStr,"Band B"); break;
		case BAND_E:    sprintf(bandStr,"Band E"); break;
		case BAND_F:    sprintf(bandStr,"Band F"); break;
		case BAND_RACE: sprintf(bandStr,"Race  "); break;
		default:        sprintf(bandStr,"User  "); break;
	}

	if (vtxData.channel < 40) {
		frequency = pgm_read_word(&VTX_FREWUENCY_TABLE[vtxData.channel]);
		channel = (vtxData.channel % 8) + 1;
	}
	else {
		frequency = vtxData.frequency;
		channel = frequency;
	}

	if (vtxData.mode & VTX_PIT_MODE_RUNNING)
	txPower = 1;
	else {
		switch (vtxData.powerLevel) {
			case VTX_25mW:  txPower = 25;		break;
			case VTX_200mW: txPower = 200;		break;
			case VTX_500mW: txPower = 500;		break;
			case VTX_800mW: txPower = 800;		break;
			default:        txPower = 0;		break;
		}
	}

	snprintf_P(&menuScreen[0][0],MENU_COLS,PSTR("VTX Settings:       "));
	snprintf_P(&menuScreen[1][0],MENU_COLS,PSTR(" Freq   : %1u.%03u Ghz"), frequency / 1000, frequency % 1000);
	snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Band   : %s       "), bandStr);
	snprintf_P(&menuScreen[3][0],MENU_COLS,PSTR(" Channel: %-4u        "), channel);
	snprintf_P(&menuScreen[4][0],MENU_COLS,PSTR(" Power  : %u mW     "), txPower);
}

void DisplayAccGyro(const struct menuItem_t *menu) {
	Menu_Clear();
	
	snprintf_P(&menuScreen[0][0],MENU_COLS,PSTR("Sensor Outputs...  "));
	snprintf_P(&menuScreen[1][0],MENU_COLS,PSTR("      X     Y     Z"));
	snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR("A%4.1f%4.1f%4.1f         "), (float)(data.packet.angle[0] / 10), (float)(data.packet.angle[1] / 10), (float)(data.packet.angle[2] / 10));
	snprintf_P(&menuScreen[3][0],MENU_COLS,PSTR("G%6d%6d%6d"), data.packet.rawGyro[0], data.packet.rawGyro[1], data.packet.rawGyro[2]);
}

void DisplaySaveScreen(const struct menuItem_t *menu) {
	Menu_Clear();
	
	snprintf_P(&menuScreen[(MENU_ROWS >> 1) - 1][0],MENU_COLS,PSTR("  --------------   "));
	snprintf_P(&menuScreen[(MENU_ROWS >> 1) - 0][0],MENU_COLS,PSTR("  | Data Saved |   "));
	snprintf_P(&menuScreen[(MENU_ROWS >> 1) + 1][0],MENU_COLS,PSTR("  --------------   "));
}

void DisplayTime(const struct menuItem_t *menu) {
	Menu_Clear();
	
	snprintf_P(&menuScreen[0][0],MENU_COLS,PSTR("Custom Menu....     "));
	snprintf_P(&menuScreen[1][0],MENU_COLS,PSTR("                    "));
	snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR("  Time is:          "));
	snprintf_P(&menuScreen[3][0],MENU_COLS,PSTR("    %3d:%02d:%02d   "), hourCounter, minCounter, secCounter);
}


void DisplayLed(const struct menuItem_t *menu) {
	Menu_Clear();
	
	snprintf_P(&menuScreen[0][0],MENU_COLS,PSTR("Selecting Colour:   "));
	snprintf_P(&menuScreen[1][0],MENU_COLS,PSTR("                    "));
	snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Red   Green  Blue  "));
	snprintf_P(&menuScreen[3][0],MENU_COLS,PSTR(" 0x%02X  0x%02X   0x%02X  "), nvMem.manualCol.r, nvMem.manualCol.g, nvMem.manualCol.b);
}

void DisplayRssiCalibrate(const struct menuItem_t *menu) {
	uint32_t rssiMath;
	
	Menu_Clear();
	
	snprintf_P(&menuScreen[0][0],MENU_COLS,PSTR("Calibrate RSSI...   "));
	snprintf_P(&menuScreen[1][0],MENU_COLS,PSTR("                    "));
	snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" RSSI :   0%%        "));
	//snprintf_P(&menuScreen[3][0],MENU_COLS,PSTR(" adc   %3d/%3d      "), rssi.avg, nvMem.rssiMaxAdc);
	//snprintf_P(&menuScreen[4][0],MENU_COLS,PSTR(" Rc[%d] %3d/%3d        "), nvMem.rssiRcChan, data.packet.aux[nvMem.rssiRcChan], nvMem.rssiMaxRc);
	
	switch (nvMem.rssiSource) {
		case RSSI_ADC:
			rssiMath = rssi.avg;
			rssiMath *= 100;
			rssiMath /= nvMem.rssiMaxAdc;
			snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" RSSI : %3d%%        "), rssiMath);
			break;
			
		case RSSI_RC:
			rssiMath = BIND(data.packet.aux[nvMem.rssiRcChan], 0, 1000);
			rssiMath *= 100;
			rssiMath /= nvMem.rssiMaxRc;
			snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" RSSI : %3d%%        "), rssiMath);
			break;
			
		default:
			nvMem.rssiSource = RSSI_RC;
			nvMem.rssiMaxRc = 1000;
	}
}

void DisplayPresetColour(const struct menuItem_t *menu) {
	valueRule_t *rulePtr;
	rulePtr = MENU_READ_PTR(&menu->rule);
	
	Menu_Clear();
	
	snprintf_P(&menuScreen[0][0],MENU_COLS,PSTR("Preset Colours...  "));
	
	switch ((enum ledColors)((*rulePtr).currVal)) {
		case COL_RED:       snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Colour : Red      ")); break;
		case COL_ORANGE:    snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Colour : Orange   ")); break;
		case COL_YELLOW:    snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Colour : Yellow   ")); break;
		case COL_GREEN:     snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Colour : Green    ")); break;
		case COL_CYAN:      snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Colour : Cyan     ")); break;
		case COL_BLUE:      snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Colour : Blue     ")); break;
		case COL_PURPLE:    snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Colour : Purple   ")); break;
		default :           snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Colour : Unknown  ")); break;
	};
}

void DisplayLedRed(const struct menuItem_t *menu) {
	valueRule_t *rulePtr;
	rulePtr = MENU_READ_PTR(&menu->rule);
	
	Menu_Clear();
	
	snprintf_P(&menuScreen[0][0],MENU_COLS,PSTR("Setting Red...     "));
	snprintf_P(&menuScreen[1][0],MENU_COLS,PSTR("                   "));
	snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Red   Green  Blue "));
	snprintf_P(&menuScreen[3][0],MENU_COLS,PSTR("*0x%02X  0x%02X   0x%02X "), (uint8_t)((*rulePtr).currVal), nvMem.manualCol.g, nvMem.manualCol.b);
}

void DisplayLedGrn(const struct menuItem_t *menu) {
	valueRule_t *rulePtr;
	rulePtr = MENU_READ_PTR(&menu->rule);
	
	Menu_Clear();
	
	snprintf_P(&menuScreen[0][0],MENU_COLS,PSTR("Setting Green...   "));
	snprintf_P(&menuScreen[1][0],MENU_COLS,PSTR("                   "));
	snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Red   Green  Blue "));
	snprintf_P(&menuScreen[3][0],MENU_COLS,PSTR(" 0x%02X *0x%02X   0x%02X "), nvMem.manualCol.r, (uint8_t)((*rulePtr).currVal), nvMem.manualCol.b);
}

void DisplayLedBlu(const struct menuItem_t *menu) {
	valueRule_t *rulePtr;
	rulePtr = MENU_READ_PTR(&menu->rule);
	
	Menu_Clear();
	
	snprintf_P(&menuScreen[0][0],MENU_COLS,PSTR("Setting Blue...    "));
	snprintf_P(&menuScreen[1][0],MENU_COLS,PSTR("                   "));
	snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Red   Green  Blue "));
	snprintf_P(&menuScreen[3][0],MENU_COLS,PSTR(" 0x%02X  0x%02X  *0x%02X "), nvMem.manualCol.r, nvMem.manualCol.g, (uint8_t)((*rulePtr).currVal));
}

void DisplayBand(const struct menuItem_t *menu) {
	valueRule_t *rulePtr;
	rulePtr = MENU_READ_PTR(&menu->rule);
	
	Menu_Clear();
	
	snprintf_P(&menuScreen[0][0],MENU_COLS,PSTR("Band Select...    "));
	
	switch ((enum vtxBand_t)((*rulePtr).currVal)) {
		case BAND_A:    snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Band: A           "));    break;
		case BAND_B:    snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Band: B           "));    break;
		case BAND_E:    snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Band: E           "));    break;
		case BAND_F:    snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Band: F           "));    break;
		case BAND_RACE: snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Band: Race        "));    break;
		case BAND_USER: snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Band: User Channel"));    break;
		default:        snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Band: ?           "));    break;
	};
}

void DisplayOutputPower(const struct menuItem_t *menu) {
	valueRule_t *rulePtr;
	rulePtr = MENU_READ_PTR(&menu->rule);
	
	Menu_Clear();
	
	snprintf_P(&menuScreen[0][0],MENU_COLS,PSTR("Output Power...    "));
	
	switch ((enum vtxPower_t)((*rulePtr).currVal)) {
		case VTX_25mW:  snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Power:  25mW      "));    break;
		case VTX_200mW: snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Power: 200mW      "));    break;
		case VTX_500mW: snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Power: 500mW      "));    break;
		case VTX_800mW: snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Power: 800mW      "));    break;
		default:        snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR(" Power: ???mW      "));    break;
	};
}

void DisplayPitMode(const struct menuItem_t * menu) {
	valueRule_t *rulePtr;
	rulePtr = MENU_READ_PTR(&menu->rule);
	
	Menu_Clear();
	
	snprintf_P(&menuScreen[0][0],MENU_COLS,PSTR("Pit Mode...        "));
	snprintf_P(&menuScreen[1][0],MENU_COLS,PSTR("-Pit Mode Off      "));
	snprintf_P(&menuScreen[2][0],MENU_COLS,PSTR("-VTX Controlled    "));
	snprintf_P(&menuScreen[3][0],MENU_COLS,PSTR("-In  Range On      "));
	snprintf_P(&menuScreen[4][0],MENU_COLS,PSTR("-In  Range Auto    "));
	snprintf_P(&menuScreen[5][0],MENU_COLS,PSTR("-Out Range On      "));
	snprintf_P(&menuScreen[6][0],MENU_COLS,PSTR("-Out Range Auto    "));
	
	
	switch ((enum vtxPitMode_t)((*rulePtr).currVal)) {
		default:
		case VTX_PIT_OFF:      menuScreen[1][0] = '>'; break;
		case VTX_PIT_MANUAL:   menuScreen[2][0] = '>'; break;
		case VTX_PIT_ON_IN:    menuScreen[3][0] = '>'; break;
		case VTX_PIT_AUTO_IN:  menuScreen[4][0] = '>'; break;
		case VTX_PIT_ON_OUT:   menuScreen[5][0] = '>'; break;
		case VTX_PIT_AUTO_OUT: menuScreen[6][0] = '>'; break;
	};
}