/*
 * MenuSystem.h
 *
 * Created: 16/07/2016 4:04:17 p.m.
 *  Author: Ned
 */ 


#ifndef MENUSYSTEM_H_
#define MENUSYSTEM_H_

#include <stddef.h>
#include <stdint.h>
#include <avr/pgmspace.h>
#include "MenuConfig.h"

#ifdef STORE_MENU_IN_FLASH
#define MENU_STORAGE				PROGMEM
#define MENU_READ_PTR(Addr)		(void*)pgm_read_word(Addr)
#define MENU_STRING_TYPE			"S"
#else
#define MENU_STORAGE
#define MENU_READ_PTR(Addr)		(void*)(Addr)
#define MENU_STRING_TYPE			"s"
#endif

struct menuItem_t;
typedef void (*menuCallback_t)(const struct menuItem_t*);

enum menuDirection {
	MENU_NAV_NONE = 0,
	MENU_NAV_UP,
	MENU_NAV_DN,
	MENU_NAV_ENTER,
	MENU_NAV_EXIT
};

enum valueType {
	TYPE_UNKNOWN = 0,
	TYPE_UINT8,
	TYPE_UINT16,
	TYPE_UINT32,
	TYPE_SINT8,
	TYPE_SINT16,
	TYPE_SINT32,
	TYPE_SINT8_PERC_1DP,
	TYPE_YES_NO,
};

typedef struct valueRule_t {
	uint8_t liveUpdate;
	uint8_t wrapAround;
	uint8_t loadRealVal;
	int32_t minVal, maxVal, defVal;
	int32_t currVal, origVal;
} valueRule_t;

#define MENU_RULE(Name, Live, Wrap, loadReal, Min, Max, Default) \
valueRule_t Name = {Live, Wrap, loadReal, Min, Max, Default, 0, 0}

typedef const struct menuItem_t {
	const struct menuItem_t *next;
	const struct menuItem_t *prev;
	const struct menuItem_t *parent;
	const struct menuItem_t *child;
	const struct menuItem_t *anchor;
	uint8_t *enabled;
	const struct menuItem_t *timeoutMenu;
	struct valueRule_t *rule;
	uint16_t timeoutTime;
	void *val;
	enum valueType valType;
	menuCallback_t onEnter;
	menuCallback_t onExit;
	menuCallback_t onSave;
	menuCallback_t onChange;
	menuCallback_t onDisplay;
	const char text[];
} menuItem_t;

#define MENU_ITEM(Name, Next, Previous, Parent, Child, Anchor, Enabled, TimeoutMenu, TimeoutTime, ValRule, Value, ValueType, EnterFunc, ExitFunc, SaveFunc, ChangeFunc, DisplayFunc, Text) \
extern menuItem_t MENU_STORAGE Next;     \
extern menuItem_t MENU_STORAGE Previous; \
extern menuItem_t MENU_STORAGE Parent;   \
extern menuItem_t MENU_STORAGE Child;  \
extern menuItem_t MENU_STORAGE TimeoutMenu;  \
extern valueRule_t ValRule;  \
menuItem_t MENU_STORAGE Name = {&Next, &Previous, &Parent, &Child, &Anchor, (uint8_t*)(&Enabled), &TimeoutMenu, &ValRule, (uint16_t)(TimeoutTime), &Value, ValueType, EnterFunc, ExitFunc, SaveFunc, ChangeFunc, DisplayFunc, Text}
	

extern uint8_t NULL_VALUE;
extern valueRule_t NULL_RULE;
extern uint8_t ENABLED_MENU;
extern uint8_t DISABLED_MENU;
const extern menuItem_t MENU_STORAGE NULL_MENU;
extern char menuScreen[MENU_ROWS][MENU_COLS];

void Menu_Clear(void);
menuItem_t* Menu_GetCurrentMenu(void);
void Menu_Init(menuItem_t *initMenu, uint16_t time);
void Menu_Navigate(enum menuDirection dir, uint16_t time);
uint8_t Menu_MenuValid(void);

#endif /* MENUSYSTEM_H_ */