/*
 * GlobalData.h
 *
 * Created: 19/07/2016 6:32:06 p.m.
 *  Author: Ned
 */ 


#ifndef GLOBALDATA_H_
#define GLOBALDATA_H_

#include "..\Menu\MenuSystem.h"
#include "..\WS LED\WS2812.h"
#include "..\ADC\ADC.h"

#define REV_0	0
#define REV_1	1
#define REV_2	16

#define VTX_MIN_FREQ 5645
#define VTX_MAX_FREQ 5945
#define VTX_FREQ_SIZE (VTX_MAX_FREQ - VTX_MIN_FREQ)

enum ledColors {
	COL_RED,
	COL_ORANGE,
	COL_YELLOW,
	COL_GREEN,
	COL_CYAN,
	COL_BLUE,
	COL_PURPLE,
	COL_MAX_COLOURS,
};

enum ledSource_t {
	LED_PRESET,
	LED_CHANNEL,
	LED_USER,
	LED_VTX,
	MAX_LED_SOURCES,
};

enum rssiSource_t {
	RSSI_RC,
	RSSI_ADC,
	MAX_RSSI_SOURCES,
};

enum voltageSource_t {
	VOLT_FC,
	VOLT_ESC,
	VOLT_ADC,
	MAX_VOLT_SOURCES,
};

enum vtxBand_t {
	BAND_A = 0,
	BAND_B,
	BAND_E,
	BAND_F,
	BAND_RACE,
	BAND_USER,
	BAND_MAX_BANDS,
};

enum vtxPower_t {
	VTX_25mW = 0,
	VTX_200mW,
	VTX_500mW,
	VTX_800mW,
	VTX_MAX_LEVELS,
};

enum vtxMode_t {
	VTX_IN_RANGE_PIT_MODE  = (1 << 0),
	VTX_OUT_RANGE_PIT_MODE = (1 << 1),
	VTX_PIT_MODE_RUNNING   = (1 << 2),
	VTX_UNLOCKED	       = (1 << 3),
};

enum vtxPowerModes_t {
	VTX_AUTO = 0,
	VTX_ON,
	VTX_PIT,
	VTX_RC,
	VTX_MANUAL,
	VTX_MAX_POWER_MODES,
};

enum tbsCommand_t {
	TBS_GET_INFO = 0x01,
	TBS_SET_POWER,
	TBS_SET_CHANNEL,
	TBS_SET_FREQUENCY,
	TBS_SET_MODE,
};

enum vtxPitMode_t {
	VTX_PIT_AUTO_OUT,
	VTX_PIT_ON_OUT,
	VTX_PIT_AUTO_IN,
	VTX_PIT_ON_IN,
	VTX_PIT_MANUAL,
	VTX_PIT_OFF,
	VTX_PIT_MAX,
};

struct SavedData {
	// LED
	led_t manualCol;
	enum ledColors userPreset;
	uint8_t rcColChan;
	uint8_t ledBrightness;
	enum ledSource_t rgbSource;
	
	// RSSI
	uint8_t rssiRcChan;
	uint16_t rssiMaxRc;
	uint16_t rssiMaxAdc;
	enum rssiSource_t rssiSource;
	
	// Volts/current
	enum voltageSource_t vSource;
	int8_t tuneAdcV, tuneFcV, tuneEscV;
	int8_t tuneMah, tuneA;
	
	// VTX
	enum vtxBand_t vtxBand;
	enum vtxPower_t vtxPower;
	uint8_t vtxChannel;
	uint16_t vtxFrequency;
	enum vtxPitMode_t vtxPitMode;
	
	// Display
	uint8_t showA, showV, showMah;
	uint8_t showTemp, showTime, showRssi;
	uint8_t showFreq, showBand, showPower, showPitMode;
	
	uint8_t checksum;
};

struct tbsData_t {
	uint8_t revision;
	uint8_t channel;
	uint8_t powerLevel;
	uint8_t mode;
	uint16_t frequency;
	uint8_t errorCount;
	uint16_t rxBaud, txBaud;
	uint8_t connected;
	uint8_t seenIt;
};

struct tbsGetInfoTx_t {
	uint16_t start;
	uint8_t command;
	uint8_t length;
	uint8_t crc;
};

struct tbsGetInfoRx_t {
	uint16_t start;
	uint8_t command:3;
	uint8_t revision:5;
	uint8_t length;
	uint8_t channel;
	uint8_t powerLevel;
	uint8_t mode;
	uint16_t frequency;
	uint8_t crc;
	uint8_t endByte;
};

struct tbsSetPower_t {
	uint16_t start;
	uint8_t command;
	uint8_t length;
	uint8_t powerLevel;
	uint8_t crc;
	//uint8_t endByte;
};

struct tbsSetChannel_t {
	uint16_t start;
	uint8_t command;
	uint8_t length;
	uint8_t channel;
	uint8_t crc;
	//uint8_t endByte;
};

struct tbsSetFrequency_t {
	uint16_t start;
	uint8_t command;
	uint8_t length;
	uint16_t frequency;
	uint8_t crc;
	//uint8_t endByte;
};

struct tbsSetMode_t {
	uint16_t start;
	uint8_t command;
	uint8_t length;
	uint8_t mode;
	uint8_t crc;
	//uint8_t endByte;
};

struct tbsUnknownPacket_t {
	uint16_t start;
	uint8_t command;
	uint8_t length;
	uint8_t data[10];
	uint8_t crc;
};

struct escStruct_t {		// 10 Each
	uint16_t temp;
	uint16_t voltage;
	uint16_t current;
	uint16_t usedAh;
	uint16_t rpm;
};

// Kiss packet starts with 0x05, followed by a byte of length. Then the data (with length of length) and followed by 8bit CRC.
// packet is 3 bytes longer than packet length due to start, length and crc byte. CRS is data (not including start, lenth and crc) added together in uint32_t, and divided by length
union kissData_t {
	uint8_t array[157];
	struct {
		uint8_t start;
		uint8_t length;
		uint8_t data[154];
		uint8_t checksum;
	} checksum;
	struct {
		uint8_t start;			//
		uint8_t length;			//
		int16_t channels[4];		// 8
		int16_t aux[4];			// 16
		uint8_t armed;			// 17
		uint16_t lipoV;			// 19
		uint16_t rawGyro[3];		// 25
		uint16_t rawAcc[3];		// 31
		int16_t angle[3];		// 37
		uint16_t i2cErrors;		// 39
		uint16_t calibGyroDone;	// 41
		uint8_t  failsafe;		// 42
		uint16_t debugThing;		// 44
		uint8_t  blank1;			// 45 always 0
		uint16_t gyroRaw[3];		// 51
		uint16_t accRaw[3];		// 57
		uint16_t accTrim[2];		// 61
		uint16_t accAng[2];		// 65
		uint8_t  mode;			// 66 0 = acro, 1 = level, 2 = 3D
		uint16_t debugThing2;	// 68
		uint16_t pwmOutputs[6];	// 80
		uint16_t debug2Thing;	// 82
		uint8_t  idleTime;		// 83
		struct escStruct_t esc[6];	// 143
		uint8_t  maxTemp;		// 144
		uint16_t minVolt;		// 146
		uint16_t maxAmps;		// 148
		uint16_t lipoMah;		// 150
		uint16_t maxRPM;			// 152
		uint16_t maxWatts;		// 154
		uint8_t checksum;
	} packet;
};

union kissSettings_t {
	uint8_t array[123];
	struct {
		uint8_t start;
		uint8_t length;
		uint8_t data[120];
		uint8_t checksum;
	} checksum;
	struct {
		uint8_t start;			//
		uint8_t length;			//
		int16_t channels[4];		// 8
		int16_t aux[4];			// 16
		uint8_t armed;			// 17
		uint16_t lipoV;			// 19
		uint16_t rawGyro[3];		// 25
		uint16_t rawAcc[3];		// 31
		int16_t angle[3];		// 37
		uint16_t i2cErrors;		// 39
		uint16_t calibGyroDone;	// 41
		uint8_t  failsafe;		// 42
		uint16_t debugThing;		// 44
		uint8_t  blank1;			// 45 always 0
		uint16_t gyroRaw[3];		// 51
		uint16_t accRaw[3];		// 57
		uint16_t accTrim[2];		// 61
		uint16_t accAng[2];		// 65
		uint8_t  mode;			// 66 0 = acro, 1 = level, 2 = 3D
		uint16_t debugThing2;	// 68
		uint16_t pwmOutputs[6];	// 80
		uint16_t debug2Thing;	// 82
		uint8_t  idleTime;		// 83
		struct escStruct_t esc[6];				// 143
		uint8_t  maxTemp;		// 144
		uint16_t minVolt;		// 146
		uint16_t maxAmps;		// 148
		uint16_t lipoMah;		// 150
		uint16_t maxRPM;			// 152
		uint16_t maxWatts;		// 154
		uint8_t checksum;
	} packet;
};

extern const uint16_t VTX_FREWUENCY_TABLE[40] PROGMEM;

extern struct tbsData_t vtxData;
extern union kissData_t data;
extern struct SavedData nvMem;
extern uint32_t presetColours[COL_MAX_COLOURS];
extern adc_t battV1, battV2, rssi, iSense;
extern uint8_t vtxDetected;
extern uint8_t vtxV2Detected;

void DataNvMemDefaults(void);
uint8_t DataNvMemLoad(void);
void DataNvMemSave(const struct menuItem_t *menu);

#endif /* GLOBALDATA_H_ */