/*
 * GlobalData.c
 *
 * Created: 19/07/2016 6:31:49 p.m.
 *  Author: Ned
 */ 

#include <string.h>
#include <avr/eeprom.h>
#include "..\WS LED\WS2812.h"
#include "GlobalData.h"
#include "..\ADC\ADC.h"

struct tbsData_t vtxData;
struct SavedData nvMem;
union kissData_t data;
adc_t battV1, battV2, rssi, iSense;
uint32_t presetColours[COL_MAX_COLOURS] = {RED, ORANGE, YELLOW, GREEN, CYAN, BLUE, PURPLE};
uint8_t vtxDetected;
uint8_t vtxV2Detected;

const uint16_t VTX_FREWUENCY_TABLE[40] PROGMEM = {
	5865, 5845, 5825, 5805, 5785, 5765, 5745, 5725, // BAND_A
	5733, 5752, 5771, 5790, 5809, 5828, 5847, 5866, // BAND_B
	5705, 5685, 5665, 5645, 5885, 5905, 5925, 5945, // BAND_E
	5740, 5760, 5780, 5800, 5820, 5840, 5860, 5880, // BAND_F
	5658, 5695, 5732, 5769, 5806, 5843, 5880, 5917, // BAND_RACE
};

static uint8_t CalcChecksum(uint8_t *data, uint16_t dataSize) {
	uint8_t checksum = 0;
	
	while (--dataSize)
		checksum ^= *data++;
		
	return checksum;
}

void DataNvMemDefaults(void) {
	// LED
	nvMem.rgbSource = LED_VTX;
	nvMem.userPreset = COL_BLUE;
	nvMem.rcColChan = 2;
	nvMem.ledBrightness = 50;
	nvMem.manualCol.r = 12;
	nvMem.manualCol.g = 34;
	nvMem.manualCol.b = 56;
	
	// RSSI
	nvMem.rssiMaxRc = 1000;
	nvMem.rssiMaxAdc = 168;
	nvMem.rssiRcChan = 3;
	nvMem.rssiSource = RSSI_RC;
	
	// Volts
	nvMem.vSource = VOLT_FC;
	nvMem.tuneAdcV = nvMem.tuneFcV = nvMem.tuneEscV = 0;
	nvMem.tuneMah = nvMem.tuneA = 0;
	
	//VTX
	nvMem.vtxPitMode = VTX_PIT_AUTO_IN;
	nvMem.vtxBand = BAND_A;
	nvMem.vtxChannel = 1;
	nvMem.vtxPower = VTX_25mW;
	nvMem.vtxFrequency = 5865;	// band a, chan 1
	
	// Show
	nvMem.showA = nvMem.showV = nvMem.showMah = 1;
	nvMem.showTemp = nvMem.showTime = nvMem.showRssi = 1;
	nvMem.showFreq = nvMem.showBand = nvMem.showPower = nvMem.showPitMode = 1;
	
	nvMem.checksum = CalcChecksum((uint8_t*)&nvMem, sizeof(nvMem));
}

void DataNvMemSave(const struct menuItem_t *menu) {
	struct SavedData tempData;
	uint8_t retries = 5;
	
	nvMem.checksum = CalcChecksum((uint8_t*)&nvMem, sizeof(nvMem));
	
	do {
		eeprom_busy_wait();
		eeprom_update_block(&nvMem, (void*)0x00, sizeof(nvMem));
		eeprom_busy_wait();
		eeprom_read_block(&tempData, (void*)0x100, sizeof(struct SavedData));
	} while (--retries && (memcmp(&nvMem, &tempData, sizeof(struct SavedData))));
}

uint8_t DataNvMemLoad(void) {
	struct SavedData tempData;
	uint8_t retries = 5;
	
	do {
		eeprom_busy_wait();
		eeprom_read_block(&tempData, (void*)0x00, sizeof(struct SavedData));
	} while (--retries && (CalcChecksum((uint8_t*)&tempData, sizeof(struct SavedData)) != tempData.checksum));
	
	if (retries) {
		memcpy(&nvMem, &tempData, sizeof(tempData));
	}
	
	return retries;
}