/*
 * RC_Kiss_FC_PDB_FW.c
 *
 * Created: 24/06/2016 10:16:07 a.m.
 *  Author: Ned
 */ 


#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include <util/delay.h>
#include "Header Files/includes.h"
#include "Memory\GlobalData.h"

#include "Menu/MenuLayout.h"

#define REQUEST_TELEMETRY	0x20
#define START_OF_PACKET		0x05

#define THROTTLE_CHAN		0
#define ROLL_CHAN			1
#define PITCH_CHAN			2
#define YAW_CHAN			3
#define R_STICK_LEFT		0x02
#define R_STICK_RIGHT		0x20
#define R_STICK_UP			0x40
#define R_STICK_DN			0x04
#define L_STICK_UP			0x10
#define L_STICK_LEFT		0x08
#define L_STICK_RIGHT		0x80
#define ARM_CHAN			1
#define RSSI_CHAN			3


#define SWAP_ENDIAN16(val)	(((uint16_t)(val) << 8) | ((uint16_t)(val) >> 8))

union kissData_t tempData;
uint8_t dataReceived;
struct tbsUnknownPacket_t tempSoftData;
uint8_t softReceived;

enum rxEnum_T  {
	RX_IDLE = 0,
	RX_PACKET_START,
	RX_PACKET_COMMAND,
	RX_PACKET_LENGTH,
	RX_DATA,
	RX_CHECKSUM,
	RX_CHECK_PASS,
	RX_CHECK_FAIL,
} rxState, softState;

enum autoBaud_t {
	NO_LOCK = 0,
	TX_LOCK,
	FULL_LOCK,
} autoSoftBaud;

uint8_t TimeYet(uint16_t compTime);

uint8_t tempByte;
uint16_t tempInt16;
uint32_t tempInt32;
char tempString[32];
led_t tempLed;

uint8_t lastArm, lastSoftState;

struct buttonsModule rcButtons;
uint8_t buttonLogic;

char battVStr[8];
char battIStr[8];
char battMStr[8];
char timeStr[12];
char tempStr[8];
char rssiStr[8];
char vtxStr[32];

volatile uint16_t isrTime;
uint16_t currentTime;
uint16_t updateLed;
uint16_t timeADC, timeReqTelem, timeLastByte, timeLastSoftByte, osdReboot;
uint16_t softSerialTimeout;
uint16_t testNed;
uint16_t timeCounter, secCounter, minCounter, hourCounter;
uint16_t updateScreen, outputScreen, clearLogo;
uint16_t updateButtons;
uint16_t pollVTX, receiveVTX;


void SetupPorts(void) {
	MOSI_DDR = OUT;
	MISO_DDR = IN;
	MISO_PUP = TRUE;
	SCK_DDR = OUT;
	LED_DDR = OUT;
	SS_DDR = OUT;
	SS = HIGH;
	
	VSYNC_DDR = IN;
	VSYNC_PUP = TRUE;
	
	RX_DDR = IN;
	RX_PUP = TRUE;
	TX_DDR = OUT;
}

void SetupTimers(void) {
	TCCR0A = (1 << WGM01) | (0 << WGM00);
	TCCR0B = (0 << WGM02) | (0x03 << CS00);
	OCR0A = (((F_CPU / 64) / 1000) - 1);
	TIMSK0 = (1 << OCIE0A);
}

void Button_Handler(struct buttonsModule *const module, uint32_t buttons) {
	
	if (!Menu_MenuValid()) {
		if (!data.packet.armed) {
			if (buttons == (L_STICK_RIGHT | L_STICK_UP)) {
				Menu_Init(&Menu_Sens, currentTime);
			}
			if (buttons == (L_STICK_LEFT | L_STICK_UP)) {
				Menu_Init(&Menu_Rev, currentTime);
			}
			if (buttons == (L_STICK_UP)) {
				Menu_Init(&Menu_VTX, currentTime);
			}
			if (buttons & (R_STICK_UP | R_STICK_DN | R_STICK_RIGHT)) {
				Menu_Init(&Menu_1, currentTime);
			}
		}
	}
	else {
		if (data.packet.armed)	{
			Menu_Init(&NULL_MENU, currentTime);
		}
		else {
			switch (buttons) {
				case R_STICK_UP:	Menu_Navigate(MENU_NAV_UP,    currentTime);   break;
				case R_STICK_DN:	Menu_Navigate(MENU_NAV_DN,    currentTime);   break;
				case R_STICK_LEFT:	Menu_Navigate(MENU_NAV_EXIT,  currentTime);   break;
				case R_STICK_RIGHT:	Menu_Navigate(MENU_NAV_ENTER, currentTime);   break;
				default:
				break;
			}
		}
	}
}

void SetupButtons(void) {
	// Create a config struct
	struct buttonsConfig config;
	
	ButtonsGetConfigDefaults(&config);													// Set config struct to defaults
	ButtonsInit(&rcButtons,  (uint16_t *)&isrTime, &config);								// Init the button module with the timer it points to, and the config you've set up
	
	ButtonsSetButton(&rcButtons, &buttonLogic, 7, 0, 7);									// Set user button index 7
	ButtonsSetButton(&rcButtons, &buttonLogic, 6, 0, 6);									// Set user button index 6
	ButtonsSetButton(&rcButtons, &buttonLogic, 5, 0, 5);									// Set user button index 5
	ButtonsSetButton(&rcButtons, &buttonLogic, 4, 0, 4);									// Set user button index 4
	ButtonsSetButton(&rcButtons, &buttonLogic, 3, 0, 3);									// Set user button index 3
	ButtonsSetButton(&rcButtons, &buttonLogic, 2, 0, 2);									// Set user button index 2
	ButtonsSetButton(&rcButtons, &buttonLogic, 1, 0, 1);									// Set user button index 1
	ButtonsSetButton(&rcButtons, &buttonLogic, 0, 0, 0);									// Set user button index 0
	ButtonsEnableButton(&rcButtons, 7);													// Enable button index 7
	ButtonsEnableButton(&rcButtons, 6);													// Enable button index 6
	ButtonsEnableButton(&rcButtons, 5);													// Enable button index 5
	ButtonsEnableButton(&rcButtons, 4);													// Enable button index 4
	ButtonsEnableButton(&rcButtons, 3);													// Enable button index 3
	ButtonsEnableButton(&rcButtons, 2);													// Enable button index 2
	ButtonsEnableButton(&rcButtons, 1);													// Enable button index 1
	ButtonsEnableButton(&rcButtons, 0);													// Enable button index 0
	
	ButtonsRegisterCallback(&rcButtons, Button_Handler, BUTTONS_CALLBACK_PRESSED);		// Register a callback for the pressed callback
	ButtonsRegisterCallback(&rcButtons, Button_Handler, BUTTONS_CALLBACK_HELD);			// Register a callback for the held callback
	
	ButtonsEnableCallback(&rcButtons, BUTTONS_CALLBACK_PRESSED);
	ButtonsEnableCallback(&rcButtons, BUTTONS_CALLBACK_HELD);
	
	ButtonsAddModule(&rcButtons);														// Add the module to the task handler
}

void ProcessSoftData(void) {
	switch (tempSoftData.command & 0x07) {
		case TBS_GET_INFO:
			vtxData.revision   = tempSoftData.command >> 3;
			vtxData.channel    = tempSoftData.data[0];
			vtxData.powerLevel = tempSoftData.data[1];
			vtxData.mode       = ((tempSoftData.data[2] & 0x0C) >> 2) | ((tempSoftData.data[2] & 0x02) << 1);
			vtxData.frequency  = tempSoftData.data[3] << 8 | tempSoftData.data[4];
			vtxData.connected  = 1;
			vtxData.seenIt     = 1;
			break;
		
		case TBS_SET_POWER:
			if (tempSoftData.data[0] <= VTX_800mW)
				vtxData.powerLevel = tempSoftData.data[0];
			else {
				switch (tempSoftData.data[0]) {
					case 7:  vtxData.powerLevel	=  VTX_25mW; break;
					case 16: vtxData.powerLevel	= VTX_200mW; break;
					case 25: vtxData.powerLevel	= VTX_500mW; break;
					case 40: vtxData.powerLevel	= VTX_800mW; break;
				}
			}
			break;
		case TBS_SET_CHANNEL:	vtxData.channel		= tempSoftData.data[0];								break;
		case TBS_SET_FREQUENCY:	vtxData.frequency	= tempSoftData.data[0] << 8 | tempSoftData.data[1];	break;
		case TBS_SET_MODE:		vtxData.mode		= tempSoftData.data[0] & 0x03;								break;
		default:
			break;	
	}
}

// Data is sent in a different endian-format, so swapping bytes here
// comment out anything not used to save processing time once you figure out what to use.
void ProcessData(void) {
	uint8_t i;
	
	tempData.packet.lipoV			= SWAP_ENDIAN16(tempData.packet.lipoV);
	tempData.packet.i2cErrors		= SWAP_ENDIAN16(tempData.packet.i2cErrors);
	tempData.packet.calibGyroDone	= SWAP_ENDIAN16(tempData.packet.calibGyroDone);
	tempData.packet.minVolt			= SWAP_ENDIAN16(tempData.packet.minVolt);
	tempData.packet.maxAmps			= SWAP_ENDIAN16(tempData.packet.maxAmps);
	tempData.packet.lipoMah			= SWAP_ENDIAN16(tempData.packet.lipoMah);
	tempData.packet.maxRPM			= SWAP_ENDIAN16(tempData.packet.maxRPM);
	tempData.packet.maxWatts			= SWAP_ENDIAN16(tempData.packet.maxWatts);
	
	tempData.packet.debugThing		= SWAP_ENDIAN16(tempData.packet.debugThing);
	tempData.packet.debugThing2		= SWAP_ENDIAN16(tempData.packet.debugThing2);
	tempData.packet.debug2Thing		= SWAP_ENDIAN16(tempData.packet.debug2Thing);
	
	for (i = 0; i < 2; i++) {
		tempData.packet.accTrim[i]  = SWAP_ENDIAN16(tempData.packet.accTrim[i]);
		tempData.packet.accAng[i]	= SWAP_ENDIAN16(tempData.packet.accAng[i]);
	}
	
	for (i = 0; i < 3; i++) {
		tempData.packet.rawGyro[i]  = SWAP_ENDIAN16(tempData.packet.rawGyro[i]);
		tempData.packet.rawAcc[i]	= SWAP_ENDIAN16(tempData.packet.rawAcc[i]);
		tempData.packet.angle[i]	= SWAP_ENDIAN16(tempData.packet.angle[i]);
		tempData.packet.gyroRaw[i]	= SWAP_ENDIAN16(tempData.packet.gyroRaw[i]);
		tempData.packet.accRaw[i]	= SWAP_ENDIAN16(tempData.packet.accRaw[i]);
	}
	
	buttonLogic = 0x00;
	for (i = 0; i < 4; i++) {
		tempData.packet.channels[i] = SWAP_ENDIAN16(tempData.packet.channels[i]);
		tempData.packet.aux[i]		= SWAP_ENDIAN16(tempData.packet.aux[i]);
		
		if (tempData.packet.channels[i] < -250)		buttonLogic |= (1 << i);
		if (tempData.packet.channels[i] >  250)		buttonLogic |= (1 << (i + 4));
	}
	
	for (i = 0; i < 6; i++) {
		tempData.packet.esc[i].temp		= SWAP_ENDIAN16(tempData.packet.esc[i].temp);
		tempData.packet.esc[i].voltage	= SWAP_ENDIAN16(tempData.packet.esc[i].voltage);
		tempData.packet.esc[i].current	= SWAP_ENDIAN16(tempData.packet.esc[i].current);
		tempData.packet.esc[i].usedAh	= SWAP_ENDIAN16(tempData.packet.esc[i].usedAh);
		tempData.packet.esc[i].rpm		= SWAP_ENDIAN16(tempData.packet.esc[i].rpm);
		tempData.packet.pwmOutputs[i]	= SWAP_ENDIAN16(tempData.packet.pwmOutputs[i]);
	}
}

uint8_t TbsCalcCRC(uint8_t *data, uint8_t length) {
	uint8_t crc = 0;
	uint8_t loop = 0;
	uint8_t bit;
	
	while (length--) {
		for (loop = 0x80; loop > 0; loop >>= 1) {
			bit = crc & 0x80;
			if (*data & loop)
				bit = !bit;
			crc <<= 1;
			if (bit) crc ^= 0xD5;
		}
		data++;
	}
	
	return crc;
}

uint8_t SendTbsPacket(uint8_t *data, uint8_t nBytes) {
	if (softState < RX_PACKET_START) {
		softState = RX_PACKET_START;
		softReceived = 0;
		timeLastSoftByte = 0;
	
		//DDRD |= 0x10;
		PORTD &= ~0x10;
		SetupSoftSerial(vtxData.txBaud);
		SoftPutC(0x00);
		SoftPutA(data, nBytes);
		SoftPutC(0x00);
	
		//pollVTX = 0;
		softSerialTimeout = MAX(1, currentTime + 200);
		return 1;
	}
	return 0;
}

void SetupVTX(const struct menuItem_t *menu) {
	struct tbsSetPower_t tbsSetPower;
	struct tbsSetChannel_t tbsSetChannel;
//	struct tbsSetFrequency_t tbsSetFrequency;
	struct tbsSetMode_t tbsSetMode;
	struct tbsGetInfoTx_t tbsGetInfoTx;
	
	tbsSetPower.start = SWAP_ENDIAN16(0xAA55);
	tbsSetPower.command = TBS_SET_POWER << 1 | 0x01;
	tbsSetPower.length = 1;
	tbsSetPower.powerLevel = vtxData.powerLevel;
	
	tbsSetMode.start = SWAP_ENDIAN16(0xAA55);
	tbsSetMode.command = TBS_SET_MODE << 1 | 0x01;
	tbsSetMode.length = 1;
	tbsSetMode.mode = vtxData.mode & 0x03;
	
// 	tbsSetFrequency.start = SWAP_ENDIAN16(0xAA55);
// 	tbsSetFrequency.command = TBS_SET_FREQUENCY << 1 | 0x01;
// 	tbsSetFrequency.length = 2;
// 	tbsSetFrequency.frequency = SWAP_ENDIAN16(vtxData.frequency);
	
	tbsSetChannel.start = SWAP_ENDIAN16(0xAA55);
	tbsSetChannel.command = TBS_SET_CHANNEL << 1 | 0x01;
	tbsSetChannel.length = 1;
	tbsSetChannel.channel = (nvMem.vtxBand * 8) + (nvMem.vtxChannel - 1);
	
		
	// Set Frequency
	switch (nvMem.vtxBand) {
		default:
			nvMem.vtxBand = 0;
		case BAND_A:
		case BAND_B:
		case BAND_E:
		case BAND_F:
		case BAND_RACE:
			tbsSetChannel.channel = (nvMem.vtxBand * 8) + (nvMem.vtxChannel - 1);
			break;
// 		case BAND_USER:
// 			tbsSetFrequency.frequency = SWAP_ENDIAN16(nvMem.vtxFrequency);
// 			break;
	}
	
	// TX Power
	switch (vtxData.revision) {
		case 0:
			switch (nvMem.vtxPower) {
				default:
				nvMem.vtxPower = VTX_25mW;
				case VTX_25mW:  tbsSetPower.powerLevel =  7; break;
				case VTX_200mW: tbsSetPower.powerLevel = 16; break;
				case VTX_500mW: tbsSetPower.powerLevel = 25; break;
				case VTX_800mW: tbsSetPower.powerLevel = 40; break;
			}
			break;
		
		default:
		case 1: tbsSetPower.powerLevel = nvMem.vtxPower;     break;
	}
	
	switch (nvMem.vtxPitMode) {
		default: nvMem.vtxPitMode = VTX_PIT_OFF;
		case VTX_PIT_OFF:    tbsSetMode.mode = VTX_PIT_MODE_RUNNING;   break;
		case VTX_PIT_ON_IN:  tbsSetMode.mode = VTX_IN_RANGE_PIT_MODE;  break;
		case VTX_PIT_ON_OUT: tbsSetMode.mode = VTX_OUT_RANGE_PIT_MODE; break;
		case VTX_PIT_MANUAL: tbsSetMode.mode = 0;                      break;
					
		case VTX_PIT_AUTO_IN:
			tbsSetMode.mode = VTX_IN_RANGE_PIT_MODE;
			if (data.packet.armed) tbsSetMode.mode |= VTX_PIT_MODE_RUNNING;
			break;
		
		case VTX_PIT_AUTO_OUT:
			tbsSetMode.mode = VTX_OUT_RANGE_PIT_MODE;
			if (data.packet.armed) tbsSetMode.mode |= VTX_PIT_MODE_RUNNING;
			break;
	}
	
	//tbsSetFrequency.crc = TbsCalcCRC((uint8_t*)(&tbsSetFrequency), 4+tbsSetFrequency.length);
	tbsSetChannel.crc   = TbsCalcCRC((uint8_t*)(&tbsSetChannel),   4+tbsSetChannel.length);	
	tbsSetPower.crc     = TbsCalcCRC((uint8_t*)(&tbsSetPower),     4+tbsSetPower.length);
	tbsSetMode.crc      = TbsCalcCRC((uint8_t*)(&tbsSetMode),      4+tbsSetMode.length);
		
	pollVTX = MAX(1, currentTime + 50);
		
	if (!vtxData.connected) {
		pollVTX = MAX(1, currentTime + 50);
			
		tbsGetInfoTx.start = SWAP_ENDIAN16(0xAA55);
		tbsGetInfoTx.command = TBS_GET_INFO << 1 | 0x01;
		tbsGetInfoTx.length = 0;
		tbsGetInfoTx.crc = TbsCalcCRC((uint8_t*)(&tbsGetInfoTx), 4+tbsGetInfoTx.length);
		SendTbsPacket((uint8_t*)(&tbsGetInfoTx), sizeof(tbsGetInfoTx));
	}
	// pit mode running and we want it off...
	else if ((vtxData.mode & VTX_PIT_MODE_RUNNING) /*&& (vtxData.mode & (VTX_IN_RANGE_PIT_MODE || VTX_OUT_RANGE_PIT_MODE))*/ && (tbsSetMode.mode & VTX_PIT_MODE_RUNNING)) {
		SendTbsPacket((uint8_t*)&tbsSetMode, sizeof(struct tbsSetMode_t));
	}
	// Pit mode setup
	else if ((vtxData.mode & (VTX_IN_RANGE_PIT_MODE || VTX_OUT_RANGE_PIT_MODE)) != (tbsSetMode.mode & (VTX_IN_RANGE_PIT_MODE || VTX_OUT_RANGE_PIT_MODE))) {
		SendTbsPacket((uint8_t*)&tbsSetMode, sizeof(struct tbsSetMode_t));
	}
	else if (tbsSetChannel.channel != vtxData.channel) {
		SendTbsPacket((uint8_t*)&tbsSetChannel, sizeof(struct tbsSetChannel_t));
	}
	else if (tbsSetPower.powerLevel != vtxData.powerLevel) {
		SendTbsPacket((uint8_t*)&tbsSetPower, sizeof(struct tbsSetPower_t));
	}
	else {
		pollVTX = MAX(1, currentTime + 1000);
			
		tbsGetInfoTx.start = SWAP_ENDIAN16(0xAA55);
		tbsGetInfoTx.command = TBS_GET_INFO << 1 | 0x01;
		tbsGetInfoTx.length = 0;
		tbsGetInfoTx.crc = TbsCalcCRC((uint8_t*)(&tbsGetInfoTx), 4+tbsGetInfoTx.length);
		SendTbsPacket((uint8_t*)(&tbsGetInfoTx), sizeof(tbsGetInfoTx));
	}
}

void UpdateLED(const struct menuItem_t *menu) {
	static led_t lastUpdate = {0};
	uint8_t loop;
	int32_t ledMath;
	led_t outLed;
	
	switch (nvMem.rgbSource) {
		case LED_VTX:
			// TBS LED colour = HSV, H = (current freq � 5645) * 1.2, S = 1, V = 255
			ledMath = pgm_read_word(&VTX_FREWUENCY_TABLE[vtxData.channel]);
			ledMath -= VTX_MIN_FREQ;
			ledMath *= 0x02FF;
			ledMath /= VTX_FREQ_SIZE;
			WS_ConvertIndex(&outLed, (uint16_t)(ledMath));
			break;
			
		case LED_USER:
			outLed.r = nvMem.manualCol.r;
			outLed.g = nvMem.manualCol.g;
			outLed.b = nvMem.manualCol.b;
			WS_NormaliseLed(&outLed, &nvMem.manualCol);
			break;
			
		case LED_PRESET:
			WS_ConvertRGB(&outLed, presetColours[nvMem.userPreset]);
			WS_NormaliseLed(&outLed, &outLed);
			break;
		
		case LED_CHANNEL:
			ledMath = data.packet.aux[nvMem.rcColChan];
			ledMath += 1000;	 // 0- 2000
			ledMath *= 0x02FF;
			ledMath /= 2000;
			WS_ConvertIndex(&outLed, (uint16_t)(ledMath));
			break;
			
		default:
			nvMem.rgbSource = LED_CHANNEL;
	}
	
	WS_LedBrightness(&outLed, nvMem.ledBrightness, 100);
	
	if (memcmp(&lastUpdate, &outLed, sizeof(led_t))) {
		cli();
		for (loop = 0; loop < PIXELS; loop++) {
			WS_SetLed(outLed);
		}
		WS_Show();
		sei();
		
		memcpy(&lastUpdate, &outLed, sizeof(led_t));
	}
}

void BlankString(char *data, uint8_t length) {
	memset(data, ' ', length);
	data[length] = 0;
}

int main(void)
{	
	PIXEL_DDR |= (1 << PIXEL_BIT);
	SetupPorts();
	SetupTimers();
	SetupSPI();
	SetupADC();	
	uart_init(UART_BAUD_SELECT_DOUBLE_SPEED(115200,F_CPU));
 	SetupSoftSerial(4800);
 	SetupButtons();
	 
	if (!DataNvMemLoad()) {
		DataNvMemDefaults();
	}
		
	vtxData.txBaud = 4800;
	vtxData.rxBaud = 5092;
		
	pollVTX = osdReboot = 50;
	updateScreen = outputScreen = timeReqTelem = osdReboot + 1000;
	
	uart_puts("Ned was here!\r\n");
	
	sei();
		
    while(1)
    {
		cli();
		currentTime = isrTime;
		sei();
				
		if (TimeYet(updateLed)) {
			updateLed = currentTime + 250;
			
			UpdateLED(NULL);
		}
		
		if (pollVTX && TimeYet(pollVTX)) {
			pollVTX = MAX(1,currentTime + 20);
			
			if (!data.packet.armed) {
				SetupVTX(0);
			}
		}
		
		// Set BAUD rate to RX after sending is complete (do once only)
		if (lastSoftState != (tempByte = SoftSerialSending())) {
			lastSoftState = tempByte;
			
			if (SoftSerialSending() == 0) {
				SetupSoftSerial(vtxData.rxBaud);
				DDRD &= ~0x10;
				PORTD &= ~0x10;
			}
		}
		
		if (lastArm != data.packet.armed) {
			lastArm = data.packet.armed;
			
			SetupVTX(0);
			
			if (data.packet.armed) {
				if (Menu_MenuValid()) 
					Menu_Init(&NULL_MENU, currentTime);
			}
			else {
				Menu_Init(&Menu_Logo, currentTime);
			}
		}
		
		if (osdReboot && TimeYet(osdReboot)) {
			osdReboot = 0;
			SetupOSD();
			Menu_Init(&Menu_Logo, currentTime);
		}
		
		if (TimeYet(timeCounter)) {
			timeCounter += 1000;
			
			if (data.packet.armed) {
				secCounter++;
			}
			if (secCounter >= 60) {
				minCounter++;
				secCounter -= 60;
			}
			
			if (minCounter >= 60) {
				hourCounter++;
				minCounter -= 60;
			}
		}
		
		if (TimeYet(updateScreen)) {
			uint8_t loop;
			uint16_t hottestESC;
			uint32_t combinedCurrent;
			uint32_t rssiAux;
			uint32_t voltMath;
			char bandStr[10];
			char freqStr[8];
			char powerStr[8];
			uint16_t frequency;
			
			updateScreen = currentTime + 250;
			outputScreen = currentTime;
			
			combinedCurrent = data.packet.esc[0].current + data.packet.esc[1].current + data.packet.esc[2].current + data.packet.esc[3].current;
			
			hottestESC = MAX(data.packet.esc[0].temp, data.packet.esc[1].temp);
			hottestESC = MAX(hottestESC, data.packet.esc[2].temp);
			hottestESC = MAX(hottestESC, data.packet.esc[3].temp);
						
			switch (nvMem.rssiSource) {
				case RSSI_ADC:
					rssiAux = rssi.avg;
					rssiAux = rssiAux * 100 / nvMem.rssiMaxAdc;
					break;
				
				case RSSI_RC:
					rssiAux = BIND(data.packet.aux[nvMem.rssiRcChan], 0,1000);
					rssiAux = rssiAux * 100 / nvMem.rssiMaxRc;
					break;
				
				default:
					rssiAux = 0;
			}
			
			switch (nvMem.vSource) {
				case VOLT_ADC:
					voltMath = battV1.avg;
					voltMath *= 235; // 22k 1K5
					voltMath /= 15;
					voltMath *= 5;
					voltMath >>= 10;
					
					voltMath *= (1000+nvMem.tuneAdcV);
					voltMath /= 1000;
					break;
					
				case VOLT_ESC:
					voltMath = loop = tempByte = 0;
					for (loop = 0; loop < 6; loop++) {
						if (data.packet.esc[loop].voltage > 5) {
							voltMath += data.packet.esc[loop].voltage;
							tempByte++;
						}
					}
					if (tempByte)
						voltMath /= tempByte;
					
					voltMath *= (1000+nvMem.tuneEscV);
					voltMath /= 1000;
					break;
				
				case VOLT_FC:
				default:
					voltMath = data.packet.lipoV;
					
					voltMath *= (1000+nvMem.tuneFcV);
					voltMath /= 1000;
					break;					
			}
			
			if (vtxData.channel < 40) 
				 frequency = pgm_read_word(&VTX_FREWUENCY_TABLE[vtxData.channel]);
			else frequency = vtxData.frequency;
			
			switch (vtxData.channel / 8) {
				case BAND_A:    sprintf(bandStr,"Band A-%1u", (vtxData.channel % 8) + 1); break;
				case BAND_B:    sprintf(bandStr,"Band B-%1u", (vtxData.channel % 8) + 1); break;
				case BAND_E:    sprintf(bandStr,"Band E-%1u", (vtxData.channel % 8) + 1); break;
				case BAND_F:    sprintf(bandStr,"Band F-%1u", (vtxData.channel % 8) + 1); break;
				case BAND_RACE: sprintf(bandStr,"Race - %1u", (vtxData.channel % 8) + 1); break;
				default:        sprintf(bandStr,"User%4u", frequency); break;
			}
			
			snprintf(freqStr, sizeof(freqStr), "%4u M", frequency);

			if (vtxData.mode & VTX_PIT_MODE_RUNNING)
				sprintf(powerStr," <1 mW");
			else {
				switch (vtxData.powerLevel) {
					case VTX_25mW:  sprintf(powerStr," 25 mW"); break;
					case VTX_200mW: sprintf(powerStr,"200 mW"); break;
					case VTX_500mW: sprintf(powerStr,"500 mW"); break;
					case VTX_800mW: sprintf(powerStr,"800 mW"); break;
					default:        sprintf(powerStr,"  0 mW"); break;
				}
			}
														
			sprintf(battVStr,"%3d.%02d%c", (uint16_t)(voltMath / 100), (uint16_t)(voltMath % 100), 'v');
			sprintf(battIStr,"%3d.%02d%c", (uint16_t)(combinedCurrent / 100), (uint16_t)(combinedCurrent % 100), 'a');
			sprintf(battMStr,"%4d%c", data.packet.lipoMah, OSD_CHAR_MAH);
			sprintf(timeStr,"%3d:%02d:%02d%c", hourCounter, minCounter, secCounter, OSD_CHAR_TIME);
			sprintf(tempStr,"%4d%c", hottestESC, OSD_CHAR_DEG);
			sprintf(rssiStr,"%4d%c", (uint16_t)(rssiAux), OSD_CHAR_RSSI);
			BlankString(vtxStr, sizeof(vtxStr));
			
			if (!nvMem.showV)     BlankString(battVStr, strlen(battVStr));
			if (!nvMem.showA)     BlankString(battIStr, strlen(battIStr));
			if (!nvMem.showMah)   BlankString(battMStr, strlen(battMStr));
			if (!nvMem.showTime)  BlankString(timeStr,  strlen(timeStr));
			if (!nvMem.showTemp)  BlankString(tempStr,  strlen(tempStr));
			if (!nvMem.showRssi)  BlankString(rssiStr,  strlen(rssiStr));
			if (!nvMem.showFreq)  BlankString(freqStr,  strlen(freqStr));
			if (!nvMem.showBand)  BlankString(bandStr,  strlen(bandStr));
			if (!nvMem.showPower) BlankString(powerStr, strlen(powerStr));
			
			if (vtxData.connected) {
				if ((nvMem.showPitMode) && (vtxData.mode == (VTX_PIT_MODE_RUNNING | VTX_IN_RANGE_PIT_MODE)))
					 snprintf_P(vtxStr, sizeof(vtxStr), PSTR("   In Range Pit Mode Active   "));
				else if ((nvMem.showPitMode) && (vtxData.mode == (VTX_PIT_MODE_RUNNING | VTX_OUT_RANGE_PIT_MODE)))
					 snprintf_P(vtxStr, sizeof(vtxStr), PSTR("  Out Range Pit Mode Active   "));
				else {
					snprintf_P(vtxStr, sizeof(vtxStr), PSTR("  %s  %s    %s"), freqStr, bandStr, powerStr); 
				}
			}
		}
		
		if (TimeYet(outputScreen)) {
			outputScreen = currentTime;
			
			if (!(PIND & (1 << 2))) {
				outputScreen = updateScreen + 100;
				
				OSD_SendString(10,14,battVStr);
				OSD_SendString( 1,14,battIStr);
				OSD_SendString(22,14,battMStr);
				OSD_SendString( 8, 0,timeStr);
				OSD_SendString( 0, 0,tempStr);
				OSD_SendString(22, 0,rssiStr);
				OSD_SendString( 0, 1,vtxStr);
								
				for (tempByte = 0; tempByte < MENU_ROWS; tempByte++) OSD_SendString(6,(3 + tempByte), &menuScreen[tempByte][0]);
			}
		}
		
		// Poll for telem packet every so often
		if (TimeYet(timeReqTelem)) {
			timeReqTelem = currentTime + 50;
			
			if (rxState <= RX_PACKET_START) {
				rxState = RX_PACKET_START;
				dataReceived = 0;
				timeLastByte = 0;
				uart_putc(REQUEST_TELEMETRY);
			}
		}
		
		// If nothing has been received for a while, reset if we're mid packet
		if (timeLastByte && TimeYet(timeLastByte + 100)) {
			timeLastByte = 0;
			
			if (rxState != RX_IDLE) {
				sprintf(tempString, "RxTimeout!\r\n");
				uart_puts(tempString);
				rxState = RX_IDLE;
			}
		}
		
		// Make up TBS packet
		if (SoftSerialAvailable()) {
			tempByte = SoftGetC();
			timeLastSoftByte = MAX(1, currentTime);
			
			switch (softState) {
				case RX_PACKET_START:
					tempSoftData.start = tempSoftData.start >> 8 | (tempByte << 8);
					if (tempSoftData.start == SWAP_ENDIAN16(0xAA55)) {
						softState = RX_PACKET_COMMAND;
						LED = !LED;
					}
					break;
					
				case RX_PACKET_COMMAND:
					tempSoftData.command = tempByte;
					softState = RX_PACKET_LENGTH;
					break;
					
				case RX_PACKET_LENGTH:
					tempSoftData.length = tempByte;
					softReceived = 0;
					softState = RX_DATA;
					break;
				
				case RX_DATA:
					tempSoftData.data[softReceived++] = tempByte;
					if (softReceived >= (tempSoftData.length - 1)) softState = RX_CHECKSUM;
					break;
				
				case RX_CHECKSUM:
					softSerialTimeout = 0;
					tempSoftData.crc = tempByte;
					tempByte = TbsCalcCRC((uint8_t*)(&tempSoftData.command), tempSoftData.length + 1);
				
					if (tempSoftData.crc == tempByte) {
						ProcessSoftData();
						softState = RX_CHECK_PASS;
					}
					else softState = RX_CHECK_FAIL;
					break;
				
				default:
					softState = RX_IDLE;
			}
		}
	
		if (softState == RX_CHECK_PASS) {
			vtxData.errorCount = 0;
			softState = RX_IDLE;
		}
	
		if (softState == RX_CHECK_FAIL) {
			vtxData.errorCount++;
			pollVTX = MAX(1, currentTime + 10);
			softState = RX_IDLE;
			
			if (vtxData.errorCount > 5) {
				vtxData.connected = 0;
			}
		}
		
		// On timeout, BAUD might be wrong. Tune baud
		if (softSerialTimeout && TimeYet(softSerialTimeout)) {
			softSerialTimeout = 0;
			
			memset(&tempSoftData, 0, sizeof(tempSoftData));			
			vtxData.errorCount++;
			pollVTX = MAX(1, currentTime + 10);
			softState = RX_IDLE;
			
			if (vtxData.errorCount > 3) {
				vtxData.connected = 0;
				vtxData.errorCount = 0;
								
				if (timeLastSoftByte)
					 vtxData.rxBaud += 50;
				else vtxData.rxBaud = vtxData.txBaud += 50; 
				
				if (vtxData.txBaud > 5200)	vtxData.txBaud = 4600;
				if (vtxData.rxBaud > 5200)	vtxData.rxBaud = 4600;
			}
			outputScreen = currentTime;
		}
		
		// Make up the Kiss FC Packet
		if (uart_available()) {
			tempByte = uart_getc();
			timeLastByte = MAX(1, currentTime);
					
			switch (rxState) {
				case RX_PACKET_START:
					if (tempByte == START_OF_PACKET) {
						tempData.packet.start = tempByte;
						rxState = RX_PACKET_LENGTH;
					}
					#ifdef DEBUG
					else {
						switch (tempByte) {
							case '8':	Button_Handler(NULL, R_STICK_UP);		break;
							case '4':	Button_Handler(NULL, R_STICK_LEFT);		break;
							case '6':	Button_Handler(NULL, R_STICK_RIGHT);	break;
							case '2':
							case '5':	Button_Handler(NULL, R_STICK_DN);		break;
							case '3':	Button_Handler(NULL, L_STICK_UP);		break;
							case 'q':   data.packet.armed = !data.packet.armed; break;
						}
					}
					#endif
					break;
					
				case RX_PACKET_LENGTH:
					tempData.packet.length = tempByte;
					dataReceived = 0;
					rxState = RX_DATA;
					
					if (tempData.packet.length != 154) rxState = RX_IDLE;
					break;
					
				case RX_DATA:
					tempData.checksum.data[dataReceived++] = tempByte;
					if (dataReceived >= tempData.packet.length) rxState = RX_CHECKSUM;
					break;
					
				case RX_CHECKSUM:
					tempData.packet.checksum = tempByte;
					
					tempInt32 = 0;
					for (tempByte = 0; tempByte < tempData.packet.length; tempByte++) {
						tempInt32 += tempData.checksum.data[tempByte];
					}
					tempByte = tempInt32 / tempData.packet.length;
					
					if (tempData.packet.checksum == tempByte) {
						ProcessData();
						memcpy(&data, &tempData, sizeof(tempData));
						rxState = RX_CHECK_PASS;
					}
					else rxState = RX_CHECK_FAIL;
					break;
					
				default:
					rxState = RX_IDLE;
			}
		}
		
		if (TimeYet(updateButtons)) {
			updateButtons = currentTime + 50;
			ButtonTaskHandler();
			Menu_Navigate(MENU_NAV_NONE, currentTime);
		}
		
		// If we've passed checksum, 
		if (rxState == RX_CHECK_PASS) {
			updateButtons = currentTime;
			updateScreen = currentTime + 2;
			updateLed = currentTime + 3;
			timeReqTelem = currentTime + 5;
			rxState = RX_IDLE;
		}
		
		if (rxState == RX_CHECK_FAIL) {
			timeReqTelem = currentTime + 5;
			rxState = RX_IDLE;
		}
		
		if (TimeYet(timeADC)) {
			timeADC = currentTime + 50;	// Every 50mS
			
			// Read the raw ADC values
			battV1.raw	= ReadADC(ADC_BATT_V1);
			battV2.raw	= ReadADC(ADC_BATT_V2);
			rssi.raw		= ReadADC(ADC_RSSI);
			iSense.raw	= ReadADC(ADC_I_SENSE);
			
			// average them over time
			AverageADC(&battV1, 2);
			AverageADC(&battV2, 2);
			AverageADC(&rssi,   2);
			AverageADC(&iSense, 3);
		}
    }
}

/***** Time Yet
 * Check if a specific time has happened yet or not
 ----------
 * @param - compTime, The time you want to compare to the current time to see if that has passed yet
 * @return - boolean, gives a true/false response
 *****/
uint8_t TimeYet(uint16_t compTime) {
 	if ((compTime - currentTime - 1) > 0xF000) {	// if the time has passed (will roll around when not serviced for 4095 counts)
    	return 1;									// the time has passed
    }
	else return 0;									// or else it has not yet passed
}

ISR(TIMER0_COMPA_vect) {
	isrTime++;
}
